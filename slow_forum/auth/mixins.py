"""New auth mixins for the views"""

from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth.mixins import (
    UserPassesTestMixin,
)


class PermissionRequiredOrOwnerMixin(UserPassesTestMixin):
    """Restrict access to model instance if required permission or owner"""
    owner_field_name = 'owner'
    permission_required = None

    def get_permission_required(self):
        """Get the permission_required field, can be override"""
        if self.permission_required is None:
            raise ImproperlyConfigured(
                f'You need to define {self.__class__.__name__}.permission_required,'
                f'or override {self.__class__.__name__}.get_permission_required().'
            )
        if isinstance(self.permission_required, str):
            return (self.permission_required,)
        else:
            return self.permission_required

    def user_has_permission(self):
        """Check if user have permission, can be override"""
        return self.request.user.has_perms(
            self.get_permission_required()
        )

    def user_is_owner(self):
        """Chec if user is owner of the instance, can be override"""
        instance = self.get_object()
        owner = getattr(instance, self.owner_field_name)
        return owner.pk == self.request.user.pk
    
    def test_func(self):
        """Test if usre has permission or is owner"""
        # Test if user has permission
        if self.user_has_permission():
            return True
        
        # Test if user is owner
        if self.user_is_owner():
            return True

        # If here, user have no access right
        return False
