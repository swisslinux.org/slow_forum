from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext_lazy

from .models import (
    Topic,
    TopicReply,
    TopicCategory,
    UserProfile,
    UserProfileExternalLinks,
    Banner,
)


# Filters classes


class ExpiredDisplayToListFilter(admin.SimpleListFilter):
    """Filter based on display_to field expiration"""
    title = _('Expired')

    parameter_name = 'expired'

    def lookups(self, request, model_admin):
        """
        Return the list of choices in the menu
        """
        return [
            ('yes', _('Yes')),
            ('no', _('No')),
        ]

    def queryset(self, request, queryset):
        """
        Return the filtered querysed, based on the value selected by user
        """
        if self.value() == 'yes':
            return queryset.filter(
                display_to__lte=timezone.now(),
            )
        if self.value() == 'no':
            return queryset.exclude(
                display_to__lte=timezone.now(),
            )


# Admin classes

@admin.register(TopicCategory)
class TopicCategoryAdmin(admin.ModelAdmin):
    """Gow the TopicCategory model can be managen from admin"""
    list_display = (
        'title',
        'priority',
        'created_by',
        'created_on',
        'topics_count',
        'replies_count',
        'edited',
    )
    readonly_fields = (
        'topics_count',
        'replies_count',
        'latest_message_created_on',
    )
    fieldsets = (
        (
            None,
            {
                'fields': ('title', 'short_description', 'full_description'),
            },
        ),
        (
            _('Category metadata'),
            {
                'fields': (
                    'priority',
                    'created_by',
                    'created_on',
                    'topics_count',
                    'replies_count',
                    'latest_message_created_on',
                ),
            },
        ),
        (
            _('Edition infos'),
            {
                'fields': (
                    'edited',
                    'edited_by',
                    'edited_on',
                ),
            },
        ),
    )


class TopicReplyInline(admin.StackedInline):
    """Topic replies as tabular inline"""
    model = TopicReply
    readonly_fields = ('message_html',)
    fieldsets = (
        (
            None,
            {
                'fields': ('message', 'message_html'),
            },
        ),
        (
            _('Reply metadata'),
            {
                'fields': ('created_by', 'created_on', 'topic',),
            },
        ),
        (
            _('Edition infos'),
            {
                'fields': (
                    'edited',
                    'edited_by',
                    'edited_on',
                ),
            },
        ),
    )


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    """How the Topic model can be managed from admin"""
    list_display = (
        'subject',
        'created_by',
        'created_on',
        'replies_count',
        'edited',
    )
    readonly_fields = ('replies_count', 'message_html')
    fieldsets = (
        (
            None,
            {
                'fields': ('subject', 'message', 'message_html'),
            },
        ),
        (
            _('Topic metadata'),
            {
                'fields': ('created_by', 'created_on', 'replies_count'),
            },
        ),
        (
            _('Edition infos'),
            {
                'fields': (
                    'edited',
                    'edited_by',
                    'edited_on',
                ),
            },
        ),
    )
    inlines = (
        TopicReplyInline,
    )


class UserProfileExternalLinksInline(admin.StackedInline):
    """UserProfileExternalLinks as tabular inline"""
    model = UserProfileExternalLinks


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    """User profile inline admin"""
    model = UserProfile
    inlines = (UserProfileExternalLinksInline,)
    list_display = (
        'nickname',
        'user',
        'edited',
    )
    fieldsets = (
        (
            None,
            {
                'fields': ('user', 'nickname',),
            },
        ),
        (
            _('Personal infos'),
            {
                'fields': ('abstract',),
            },
        ),
        (
            _('Edition infos'),
            {
                'fields': (
                    'edited',
                    'edited_by',
                    'edited_on',
                ),
            },
        ),
    )


class UserAdmin(BaseUserAdmin):
    """User admin, extend the basic user admin with profile"""
    actions = ('create_missing_profiles',)
    list_filter = BaseUserAdmin.list_filter + (
        ('profile', admin.EmptyFieldListFilter),
    )

    @admin.action(
        description='Create missing profiles for selected users',
        permissions=('add',),
    )
    def create_missing_profiles(self, request, queryset):
        """Create missing profiles"""
        user_count = UserProfile.create_missing(queryset)
        if user_count == 0:
            self.message_user(
                request,
                _('No missing User Profiles'),
                messages.SUCCESS,
            )
        else:
            self.message_user(
                request,
                ngettext_lazy(
                    'Successfully create {user_count} missing User Profile',
                    'Successfully create {user_count} missing User Profiles',
                    user_count,
                ).format(user_count=user_count),
                messages.SUCCESS,
            )


# Replace the original UserAdmin by mine
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    """How the Banner model can be managed from admin"""
    list_display = (
        'title',
        'priority',
        'display_from',
        'display_to',
    )
    list_filter = (ExpiredDisplayToListFilter,)
    actions = ('delete_expired',)
    readonly_fields = (
        'message_html',
    )
    fieldsets = (
        (
            None,
            {
                'fields': ('title', 'message'),
            },
        ),
        (
            _('Banner metadata'),
            {
                'fields': ('priority', 'display_from', 'display_to'),
            },
        ),
    )

    @admin.action(
        description='Delete expired selected banners',
        permissions=('delete',),
    )
    def delete_expired(self, request, queryset):
        """Delee the expired banners in the selected ones"""
        Banner.delete_expired(
            queryset=queryset,
        )

