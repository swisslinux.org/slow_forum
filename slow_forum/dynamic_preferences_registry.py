from django.utils.translation import gettext_lazy as _

from dynamic_preferences.types import (
    StringPreference,
    LongStringPreference,
    FilePreference,
)
from dynamic_preferences.preferences import (
    Section,
)
from dynamic_preferences.registries import (
    global_preferences_registry
)


# Default values

DEFAULT_FORUM_NAME = 'Slow forum'
DEFAULT_WELCOME_MESSAGE = """Welcome to your Slow forum.

Please, edit your settings."""
DEFAULT_FORUM_DESCRIPTION = """This forum is made with Slow forum.

To set its description, please edit your settings."""
DEFAULT_FORUM_RULES = """You can set your forum rules in your settings."""

# Section

general_section = Section(
    'general',
    _('General'),
)


look_section = Section(
    'look',
    _('Look'),
)


# Forum general preferences

@global_preferences_registry.register
class ForumName(StringPreference):
    """The name of the forum"""
    section = general_section
    name = 'forum_name'
    default = DEFAULT_FORUM_NAME
    verbose_name = _('Forum name')


@global_preferences_registry.register
class ForumLogo(FilePreference):
    """The forum logo"""
    section = general_section
    name = 'forum_logo'
    default = None
    verbose_name = _('Forum logo')


@global_preferences_registry.register
class ForumFavicon(FilePreference):
    """The forum Favicon"""
    section = general_section
    name = 'forum_favicon'
    default = None
    verbose_name = _('Forum favicon')

    
@global_preferences_registry.register
class WelcomeMessage(LongStringPreference):
    """A welcome message for forum visitors"""
    section = general_section
    name = 'welcome_message'
    default = DEFAULT_WELCOME_MESSAGE
    verbose_name = _('Welcome message')
    help_text = _('A short welcome message, shown on the home page')


@global_preferences_registry.register
class ForumDescription(LongStringPreference):
    """The description of this forum"""
    section = general_section
    name = 'forum_description'
    default = DEFAULT_FORUM_DESCRIPTION
    verbose_name = _('Forum description')
    help_text = _('Explain for what or who is your forum')


@global_preferences_registry.register    
class ForumRules(LongStringPreference):
    """The rules of this forum"""
    section = general_section
    name = 'forum_rules'
    default = DEFAULT_FORUM_RULES
    verbose_name = _('Forum rules')


# Forum look preferences

# @global_preferences_registry.register
# class HeaderBackgroundColor(StringPreference):
#     """The background color of the header"""
#     section = look_section
#     name = 'header_background_color'
#     default = ''
#     verbose_name = _('Header background color')
#     help_text = _('Enter a CSS color')


# @global_preferences_registry.register
# class HeaderTextColor(StringPreference):
#     """The text color of the header"""
#     section = look_section
#     name = 'header_text_color'
#     default = ''
#     verbose_name = _('Header text color')
#     help_text = _('Enter a CSS color')
