from django.conf import settings


# Post message conversion to HTML
if not hasattr(settings, 'SLOW_MESSAGE_HTML'):
    # Set default config
    SLOW_MESSAGE_TO_HTML = {
        'HEADING_OFFSET': 2,
        'ENABLE_TOC': True,
        'ENABLE_CODE_HIGHLIGHT': True,
    }

    # Note: MARKDOWN_EXTENSIONS follow the same format than the
    # parameter "extension_configs" of the markdown.markdown() converter
    # function
    
else:
    # Use config in websit settings
    SLOW_MESSAGE_TO_HTML = settings.SLOW_MESSAGE_TO_HTML
