from django.shortcuts import (
    render,
    get_object_or_404,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin,
)
from django.views import generic
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin


from dynamic_preferences.registries import global_preferences_registry

from extra_views import (
    InlineFormSetFactory,
    UpdateWithInlinesView,
)

from slow_forum.models import (
    Topic,
    TopicReply,
    TopicCategory,
    UserProfile,
    UserProfileExternalLinks,
)
from slow_forum.forms import (
    TopicReplyForm,
    SearchForm,
)
from slow_forum.auth.mixins import (
    PermissionRequiredOrOwnerMixin,
)


# dynamic preferences manager
global_preferences = global_preferences_registry.manager()


# Personal Mixins

class RecordEditionMixin:
    """Record an object edition when update view do a form_valid()"""

    def form_valid(self, form):
        """Record an edition on the object"""
        form.instance.record_edition(
            by_user=self.request.user,
            on_datetime=timezone.now(),
        )
        return super().form_valid(form)


class SetCreatorOnCreateMixin:
    """Set the creator field to request user, only use on CreateView"""

    creator_field_name = 'created_by'
    
    def form_valid(self, form):
        """Form validation overrid to set creator field"""
        setattr(
            form.instance,
            self.creator_field_name,
            self.request.user,
        )
        return super().form_valid(form)


# Views

class IndexView(generic.TemplateView):
    """Index of the forum, list all subjects"""
    template_name = 'slow_forum/index.html'
    context_object_name = 'topics_list'

    def get_context_data(self, *args, **kwargs):
        """Override context to add topics list and replies list"""
        context = super().get_context_data(*args, **kwargs)
        context['topics_list'] = Topic.objects.order_by(
            'last_message_on'
        )[:10]
        return context


class TopicCategoryListView(generic.ListView):
    """List all categories"""
    model = TopicCategory
    context_object_name = 'topic_category_list'
    template_name = 'slow_forum/topic_category_list.html'


class TopicCategoryDetailView(generic.DetailView):
    """Detail of one category"""
    model = TopicCategory
    context_object_name = 'topic_category'
    template_name = 'slow_forum/topic_category_detail.html'


class TopicCategoryCreateView(
        LoginRequiredMixin,
        PermissionRequiredMixin,
        SuccessMessageMixin,
        SetCreatorOnCreateMixin,
        generic.edit.CreateView,
):
    """Create view of a Topic Category"""
    permission_required = ('slow_forum.add_topiccategory',)
    model = TopicCategory
    fields = (
        'title',
        'priority',
        'short_description',
        'full_description',
    )
    success_message = _('Topic subject "%(title)s" was created successfully')
    template_name = 'slow_forum/topic_category_form.html'


class TopicCategoryUpdateView(
        LoginRequiredMixin,
        PermissionRequiredMixin,
        RecordEditionMixin,
        SuccessMessageMixin,
        generic.edit.UpdateView,
):
    """Update view of Topic Category"""
    permission_required = (
        'slow_forum.change_topiccategory',
    )
    owner_field_name = 'created_by'
    model = TopicCategory
    fields = (
        'title',
        'priority',
        'short_description',
        'full_description',
    )
    context_object_name = 'topic_category'
    success_message = _('Topic category "%(title)s" was edited successfully')
    template_name = 'slow_forum/topic_category_form.html'


class TopicCategoryDeleteView(
        LoginRequiredMixin,
        PermissionRequiredMixin,
        SuccessMessageMixin,
        generic.edit.DeleteView,
):
    """Delete view for a Topic Category"""
    permission_required = (
        'slow_forum.delete_topiccategory',
    )
    model = TopicCategory
    context_object_name = 'topic_category'
    template_name = 'slow_forum/topic_category_confirm_delete.html'
    success_url = reverse_lazy(
        'slow_forum:topic-category-list',
    )
    success_message = _('Topic category was deleted successfully')


class TopicDetailView(generic.DetailView):
    """Details of topic, with metadata and all its posts"""
    model = Topic
    template_name = 'slow_forum/topic_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(
            *args,
            **kwargs,
        )
        context['topic_reply_form'] = TopicReplyForm()
        return context


class TopicCreateView(
        LoginRequiredMixin,
        PermissionRequiredMixin,
        SuccessMessageMixin,
        SetCreatorOnCreateMixin,
        generic.edit.CreateView,
):
    """Create view of a topic"""
    permission_required = ('slow_forum.add_topic',)
    model = Topic
    fields = ('category', 'subject', 'message')
    success_message = _('Topic "%(subject)s" was created successfully')

    def get_context_data(self, *args, **kwargs):
        """Override to add category"""
        context = super().get_context_data(*args, **kwargs)
        if self.kwargs.get('topic_category_pk'):
            category = get_object_or_404(
                TopicCategory,
                pk=self.kwargs.get('topic_category_pk'),
            )
            context['category'] = category
        return context
    
    def get_form_kwargs(self):
        """Override to add default topic category if set by url"""
        form_kwargs = super().get_form_kwargs()

        if self.kwargs.get('topic_category_pk'):
            category = get_object_or_404(
                TopicCategory,
                pk=self.kwargs.get('topic_category_pk'),
            )
            form_kwargs['initial']['category'] = category.pk
            
        return form_kwargs


class TopicUpdateView(LoginRequiredMixin,
                      PermissionRequiredOrOwnerMixin,
                      RecordEditionMixin,
                      SuccessMessageMixin,
                      generic.edit.UpdateView):
    """Update view of a topic"""
    permission_required = (
        'slow_forum.change_topic',
    )
    owner_field_name = 'created_by'
    model = Topic
    fields = ('category', 'subject', 'message')
    success_message = _('Topic "%(subject)s" was edited successfully')


class TopicDeleteView(
        LoginRequiredMixin,
        PermissionRequiredMixin,
        SuccessMessageMixin,
        generic.edit.DeleteView,
):
    """Delete a Topic object"""
    permission_required = (
        'slow_forum.delete_topic',
    )
    model = Topic
    success_url = reverse_lazy(
        'slow_forum:index',
    )
    success_message = _('Topic was deleted successfully')


class TopicReplyCreateView(LoginRequiredMixin,
                           PermissionRequiredMixin,
                           SuccessMessageMixin,
                           generic.edit.CreateView):
    """Create view of a topic reply"""
    permission_required = ('slow_forum.add_topicreply',)
    model = TopicReply
    fields = ('message',)
    template_name = 'slow_forum/topic_reply_form.html'
    success_message = _('Reply was created successfully')

    def get_context_data(self, *args, **kwargs):
        """Override get_context_data() to add the topic"""
        context = super(TopicReplyCreateView, self).get_context_data(
            *args,
            **kwargs,
        )
        context['topic'] = get_object_or_404(
            Topic,
            pk=self.kwargs['topic_pk'],
        )
        return context

    def form_valid(self, form):
        """Form validation"""
        topic = get_object_or_404(
            Topic,
            pk=self.kwargs['topic_pk'],
        )
        form.instance.topic = topic
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class TopicReplyUpdateView(
        LoginRequiredMixin,
        PermissionRequiredOrOwnerMixin,
        RecordEditionMixin,
        SuccessMessageMixin,
        generic.edit.UpdateView,
):
    """Update a topic reply"""
    permission_required = ('slow_forum.change_topicreply',)
    owner_field_name = 'created_by'
    model = TopicReply
    form_class = TopicReplyForm
    template_name = 'slow_forum/topic_reply_form.html'
    success_message = _('Reply was edited successfully')

    def get_context_data(self, *args, **kwargs):
        """Override get_context_data() to add the topic"""
        context = super(TopicReplyUpdateView, self).get_context_data(
            *args,
            **kwargs,
        )
        context['topic'] = get_object_or_404(
            Topic,
            pk=self.kwargs['topic_pk'],
        )
        return context


class TopicReplyDeleteView(
        LoginRequiredMixin,
        PermissionRequiredMixin,
        SuccessMessageMixin,
        generic.edit.DeleteView,
):
    """Delete a Topic reply object"""
    permission_required = (
        'slow_forum.delete_topicreply',
    )
    model = TopicReply
    template_name = 'slow_forum/topic_reply_confirm_delete.html'
    success_message = _('Reply was deleted successfully')

    def get_success_url(self):
        """Return the url to the topic"""
        topic = get_object_or_404(
            Topic,
            pk=self.kwargs.get('topic_pk')
        )
        return topic.get_absolute_url()


class UserProfileListView(generic.ListView):
    """List all user profiles"""
    model = UserProfile
    template_name = 'slow_forum/user_profile_list.html'


class UserProfileDetailView(generic.DetailView):
    """Detail of one User Profile"""
    model = UserProfile
    context_object_name = 'user_profile'
    template_name = 'slow_forum/user_profile_detail.html'
    extra_context = {
        'tab': 'general',
    }
    tab_names = [
        'general',
        'topics_list',
        'topics_replies_list'
    ]

    def get_context_data(self, **kwargs):
        """Override context generation to add tabs data"""
        context = super().get_context_data(**kwargs)

        # Get tab name
        tab_name = self.kwargs.get('tab_name', 'general')

        # Check tab name is on of available tabs
        if tab_name not in self.tab_names:
            tab_name = self.tab_names[0]

        context['tab_name'] = tab_name

        # Additional data for each tab
        if tab_name == 'topics_list':
            context[
                'topics_list'
            ] = self.object.user.topic_set.all()

        if tab_name == 'topics_replies_list':
            context[
                'topics_replies_list'
            ] = self.object.user.topicreply_set.all()
            
        return context


class UserProfileExternalLinksInline(InlineFormSetFactory):
    """An inline of User Profile Extrernal Links"""
    model = UserProfileExternalLinks
    fields = (
        'label',
        'url',
        'priority',
    )

    
class UserProfileUpdateView(LoginRequiredMixin,
                            PermissionRequiredOrOwnerMixin,
                            RecordEditionMixin,
                            SuccessMessageMixin,
                            UpdateWithInlinesView):
    """Update of one User Profile"""
    permission_required = ('slow_forum.change_userprofile',)
    owner_field_name = 'user'
    model = UserProfile
    fields = (
        'nickname',
        'abstract',
    )
    inlines = (UserProfileExternalLinksInline,)
    template_name = 'slow_forum/user_profile_form.html'
    success_message = _(
        'User profile for %(nickname)s was edited successfully'
    )

    def get_success_url(self):
        return self.object.get_absolute_url()
    

class AboutView(
        generic.TemplateView,
):
    """Simply show informations about the forum"""
    template_name = 'slow_forum/about.html'
    extra_context = {
        'softwares_list': [
            {
                'name': 'Slow Forum',
                'website': 'https://framagit.org/swisslinux.org/slow_forum',
                'documentation': 'https://framagit.org/swisslinux.org/slow_forum/-/tree/main/docs',
                'source_code': 'https://framagit.org/swisslinux.org/slow_forum',
            },
            {
                'name': 'Bootstrap',
                'website': 'https://getbootstrap.com/',
                'documentation': 'https://getbootstrap.com/docs/5.3/getting-started/introduction/',
                'source_code': 'https://github.com/twbs/bootstrap',
            },
            {
                'name': 'Django',
                'website': 'https://www.djangoproject.com/',
                'documentation': 'https://docs.djangoproject.com/en/4.2/',
                'source_code': 'https://github.com/django/django',
            },
            {
                'name': 'django-extra-views',
                'website': 'https://django-extra-views.readthedocs.io/en/latest/',
                'documentation': 'https://django-extra-views.readthedocs.io/en/latest/',
                'source_code': 'https://github.com/AndrewIngram/django-extra-views',
            },
            {
                'name': 'django-dynamic-preferences',
                'website': 'https://django-dynamic-preferences.readthedocs.io/en/latest/',
                'documentation': 'https://django-dynamic-preferences.readthedocs.io/en/latest/',
                'source_code': 'https://github.com/agateblue/django-dynamic-preferences',
            },
            {
                'name': 'django-registration',
                'website': 'https://django-registration.readthedocs.io/en/latest/index.html',
                'documentation': 'https://django-registration.readthedocs.io/en/latest/index.html',
                'source_code': 'https://github.com/ubernostrum/django-registration',
            },
            {
                'name': 'Python-Markdown',
                'website': 'https://python-markdown.github.io/',
                'documentation': 'https://python-markdown.github.io/',
                'source_code': 'https://github.com/Python-Markdown/markdown/',
            },
        ],
    }
    

class FeedsView(
        generic.TemplateView,
):
    """List all the website RSS feeds"""
    template_name = 'slow_forum/feeds.html'
    extra_context = {
        'feeds': {
            'topics_all': {
                'verbose_name': _('All topics'),
                'url': reverse_lazy('slow_forum:feed-topics-all'),
            },
            'topic_replies_all': {
                'verbose_name': _('All topics replies'),
                'url': reverse_lazy('slow_forum:feed-topic-replies-all'),
            },
        },
        'categories': TopicCategory.objects.order_by('priority').all(),
    }


class SearchView(
        generic.TemplateView,
):
    """A search view, for many models

    Can do search on:
    * topic
    * topic_reply
    * user_profile

    Type of search set by view.search_type, can be set on URL with:
    views.SearchView.as_view(search_type='topic_reply')
    """
    
    template_name = 'slow_forum/search.html'
    form_class = SearchForm
    search_type = 'topic'

    def get_context_data(self, **kwargs):
        """Override get_context_data() to add.

        * A search form
        * The search query
        * The search result
        """
        # Get the initial context
        context = super().get_context_data(**kwargs)

        # Creat the form
        form = self.form_class(self.request.GET)
        context['form'] = form

        # If query is set by user
        if form.is_valid():
            if form['q'].value():
                context['query'] = form.cleaned_data['q']

        # Do search based on search type
        if self.search_type == 'topic':
            context['search_type'] = self.search_type
            context['search_results'] = self.get_topics(
                form.get_search_query_splited()
            )
        else:
            # By default, do a on topics
            context['search_type'] = 'topic'
            context['search_results'] = self.get_topics(
                form.get_search_query_splited()
            )
            
        return context

    def get_topics(self, query_splited):
        """Get the topics corresponding to splitted user query"""
        if query_splited:
            # Set the start of the search
            topic_queryset_subject = Topic.objects.all()
            topic_queryset_message = Topic.objects.all()

            # For each queries
            for query in query_splited:
                topic_queryset_subject &= Topic.objects.filter(
                    subject__icontains=query,
                )
                topic_queryset_message &= Topic.objects.filter(
                    message__icontains=query,
                )

            # Return the merge of the 2 querysets
            return (
                topic_queryset_subject | topic_queryset_message
            )
        else:
            return None
