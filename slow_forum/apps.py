from django.apps import AppConfig
from django.db.models.signals import post_save as model_post_save_signal


class SlowForumConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "slow_forum"
    verbose_name = "Slow forum"

    def ready(self):
        from slow_forum.callbacks import model_post_save_callback
        
        model_post_save_signal.connect(
            model_post_save_callback,
        )
