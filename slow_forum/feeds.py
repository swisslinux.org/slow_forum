from django.contrib.syndication.views import Feed
from django.template.defaultfilters import (
    linebreaks,
    escape,
)
from django.utils.translation import gettext_lazy as _
from django.utils.feedgenerator import Atom1Feed

from dynamic_preferences.registries import global_preferences_registry

from slow_forum.models import (
    Topic,
    TopicReply,
    TopicCategory,
)


# dynamic preferences manager
global_preferences = global_preferences_registry.manager()


# Feeds

class TopicsAllFeed(Feed):
    """All the latest topics"""
    feed_type = Atom1Feed
    link = "/"
    subtitle = _('All the latest topics, max 25')

    def title(self, obj):
        """Get the feed title"""
        title = '{}: {}'.format(
            global_preferences['general__forum_name'],
            _('Latest topics (max 25)'),
        )
        return title

    def items(self):
        """Get topics"""
        return Topic.objects.order_by('-created_by')[:25]

    def item_title(self, topic):
        """Get topic title"""
        return escape(
            topic.subject,
        )

    def item_pubdate(self, topic):
        """Get topic pubdate"""
        return topic.created_on

    def item_description(self, topic):
        """Get topic description"""
        return linebreaks(
            escape(
                topic.message,
            ),
        )

    def item_author_name(self, topic):
        """Get topic author name"""
        return escape(
            topic.created_by.profile.nickname,
        )

    def item_author_link(self, topic):
        """Get topic author link"""
        return topic.created_by.profile.get_absolute_url()

    def item_updateddate(self, topic):
        """Get topic update datetime"""
        return topic.edited_on


class TopicRepliesAllFeed(Feed):
    """All the latest topic replies"""
    feed_type = Atom1Feed
    link = "/"
    subtitle = _('All the latest topics replies, max 25')

    def title(self, obj):
        """Get the feed title"""
        title = '{}: {}'.format(
            global_preferences['general__forum_name'],
            _('Latest topics replies (max 25)'),
        )
        return title

    def items(self):
        """Get topics replies"""
        return TopicReply.objects.order_by('-created_by')[:25]

    def item_title(self, topic_reply):
        """Get topic reply title"""
        return '{}: {}'.format(
            _('Reply in topic'),
            escape(
                topic_reply.topic.subject,
            ),
        )

    def item_pubdate(self, topic_reply):
        """Get topic reply pubdate"""
        return topic_reply.created_on

    def item_description(self, topic_reply):
        """Get topic reply description"""
        return linebreaks(
            escape(
                topic_reply.message,
            ),
        )

    def item_author_name(self, topic_reply):
        """Get topic reply author name"""
        return escape(
            topic_reply.topic.created_by.profile.nickname,
        )

    def item_author_link(self, topic_reply):
        """Get topic reply author link"""
        return topic_reply.topic.created_by.profile.get_absolute_url()

    def item_updateddate(self, topic):
        """Get topic update datetime"""
        return topic.edited_on


class TopicsCategoryFeed(TopicsAllFeed):
    """Get latest topics of a category"""

    feed_type = Atom1Feed

    def get_object(self, request, category_pk):
        """Get a category from category_pk"""
        return TopicCategory.objects.get(pk=category_pk)

    def title(self, category):
        """Get feed title from category"""
        return _('{}: Category {}, latest topics (max 25)').format(
            global_preferences['general__forum_name'],
            escape(
                category.title,
            ),
        )

    def link(self, category):
        """Get feed link from category"""
        return category.get_absolute_url()

    def subtitle(self, category):
        """Get feed subtitle from category"""        
        return _('All the latest topics of category {}, max 25').format(
            escape(
                category.title,
            ),
        )

    def items(self, category):
        """Get topics"""
        return Topic.objects.filter(
            category=category
        ).order_by(
            '-created_by'
        )[:25]


class TopicRepliesCategoryFeed(TopicRepliesAllFeed):
    """Get latest topic replies of a category"""

    feed_type = Atom1Feed
    
    def get_object(self, request, category_pk):
        """Get a category from category_pk"""
        return TopicCategory.objects.get(pk=category_pk)

    def title(self, category):
        """Get feed title from category"""
        return _('{}: Category {}, latest topic replies (max 25)').format(
            global_preferences['general__forum_name'],
            escape(
                category.title,
            ),
        )

    def link(self, category):
        """Get feed link from category"""
        return category.get_absolute_url()

    def subtitle(self, category):
        """Get feed subtitle from category"""        
        return _('All the latest topic replies of category {}, max 25').format(
            escape(
                category.title,
            ),
        )

    def items(self, category):
        """Get topic replies from category"""
        return TopicReply.objects.filter(
            topic__category=category
        ).order_by(
            '-created_by'
        )[:25]


class TopicRepliesTopicFeed(TopicRepliesAllFeed):
    """Get topic replies of a topic"""

    feed_type = Atom1Feed
    
    def get_object(self, request, topic_pk):
        """Get a topic from topic_pk"""
        return Topic.objects.get(pk=topic_pk)

    def title(self, topic):
        """Get feed title from topic"""
        return _('{}: Topic "{}", latest topic replies (max 25)').format(
            global_preferences['general__forum_name'],
            escape(
                topic.subject,
            ),
        )

    def link(self, topic):
        """Get feed link from topic"""
        return topic.get_absolute_url()

    def subtitle(self, topic):
        """Get feed subtitle from topic"""        
        return _('All the latest topic replies of topic "{}", max 25').format(
            escape(
                topic.subject,
            ),
        )

    def items(self, topic):
        """Get topic replies from topic"""
        return TopicReply.objects.filter(
            topic=topic
        ).order_by(
            '-created_by'
        )[:25]
    
