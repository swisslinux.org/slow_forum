from datetime import timedelta
from django.utils import timezone

from django.contrib.auth import get_user_model
from django.contrib.auth.models import (
    Group,
    Permission,
)
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, Client
from django.utils import timezone
from django.urls import reverse

from .utils import (
    MAX_HTML_HEADING_LEVEL,
    shift_html_heading,
)
from .convert import (
    markdown_to_html
)
from slow_forum.views import (
    TopicCreateView,
    TopicUpdateView,
    TopicDeleteView,
    TopicReplyCreateView,
    TopicReplyUpdateView,
    TopicReplyDeleteView,
    TopicCategoryCreateView,
    TopicCategoryUpdateView,
    TopicCategoryDeleteView,
    UserProfileUpdateView,
)
from slow_forum.models import (
    UserProfile,
    Topic,
    TopicReply,
    TopicCategory,
    Banner,
)


# Function for repetitive tests
def test_access_logging_required(
        test_object,
        view_class,
        url,
        client=None,
):
    """Test if access to a view is blocked if user not logged.


    Parameters:
    * test_object: The object doing the test
    * view_class: The class used for the view
    * url: URL used by the cilent
    * Client: Client used to do the request, optional,
      if not defined it use test_object.client

    Return: Nothing, use test_object.assert* to do the test
    """
    if not client:
        client = test_object.client

    # Access with not logged user
    client.logout()
    response = client.get(
        url,
    )
    
    # Test we have been redirected to login view
    test_object.assertEqual(
        response.status_code,
        302,
        msg='Not logged user not return HTTP code 302'
    )
    test_object.assertIn(
        view_class().get_login_url(),
        response.url,
        msg='Not logged user not redirected to login url',
    )


def test_access_permission_required(
        test_object,
        url,
        user_without_permission,
        user_with_permission,
        client=None,
):
    """Test if access to a view is allowed for user with a certain permission.

    Parameters:
    * test_object: The object doing the test
    * url: URL used by the cilent
    * user_without_permission: A user without the permission to acces the view
    * user_with_permission: A user with the permission to acces the view
    * Client: Client used to do the request, optional,
      if not defined it use test_object.client

    Return: Nothing, use test_object.assert* to do the test
    """
    if not client:
        client = test_object.client

    # Access by user without permission
    client.logout()
    client.force_login(
        user=user_without_permission,
    )
    response = client.get(
        url,
    )
    
    # Test we have been blocked
    test_object.assertEqual(
        response.status_code,
        403,
        msg='User without permission don\'t get an HTTP code 403'
    )

    # Access by user without permission
    client.logout()
    client.force_login(
        user=user_with_permission,
    )
    response = client.get(
        url,
    )

    # Test we have access
    test_object.assertEqual(
        response.status_code,
        200,
        msg='User with permission don\'t get acces with an HTTP code 200'
    )


def test_access_owner(
        test_object,
        url,
        user_not_owner,
        user_owner,
        client=None,
):
    """Test if access to a view is allowed for user with a certain permission.

    Parameters:
    * test_object: The object doing the test
    * url: URL used by the cilent
    * user_not_owner: A user not owner of the object on the view
    * user_owner: A user owner of the object on the view
    * Client: Client used to do the request, optional,
      if not defined it use test_object.client

    Return: Nothing, use test_object.assert* to do the test
    """
    if not client:
        client = test_object.client

    # Access by user not owner
    client.logout()
    client.force_login(
        user=user_not_owner,
    )
    response = client.get(
        url,
    )
    
    # Test we have been blocked
    test_object.assertEqual(
        response.status_code,
        403,
        msg='User not owner don\'t get an HTTP code 403'
    )

    # Access by user without permission
    client.logout()
    client.force_login(
        user=user_owner,
    )
    response = client.get(
        url,
    )
    
    # Test we have access
    test_object.assertEqual(
        response.status_code,
        200,
        msg='User owner don\'t get acces with an HTTP code 200'
    )


def test_object_edition_record_with_view(
        test_object,
        edited_object,
        user,
        url,
        form_data,
        http_status_code_after_edition,
        client=None,
):
    """Test edition is recorded in case of object edition with a view

    Parameters:
    * test_object: The object doing the test
    * url: URL used by the cilent
    * user: The user who do the edition
    * form_data: The data send in the post request
    * http_status_code_after_edition: The HTTP status get after correct edition
    * Client: Client used to do the request, optional,
      if not defined it use test_object.client

    Return: Nothing, use test_object.assert* to do the test
    """
    if not client:
        client = test_object.client

    # Check that the edited object is not already edited
    test_object.assertFalse(
        edited_object.edited,
        msg='Edited object is already edited',
    )

    # Edit the edited object with the client
    client.logout()
    client.force_login(
        user=user,
    )
    response = client.post(
        url,
        form_data,
    )
    
    # Check the request is a success
    test_object.assertEqual(
        response.status_code,
        http_status_code_after_edition,
        msg='Wrong http status code after edition',
    )

    # Reload edited object from db
    edited_object.refresh_from_db()
    
    # Check the edited object edition is recorded
    test_object.assertTrue(
        edited_object.edited,
        msg='Edited object is not edited',
    )
    test_object.assertEqual(
        edited_object.edited_by,
        user,
        msg='Edited object edited_by is not correctly set',
    )
    test_object.assertTrue(
        edited_object.edited_on,
        msg='Edited object edited_on is not correctly set',
    )
    

# Test cases

# Test utils

class UtilsTests(TestCase):
    """Test the utils.py functions"""
    def test_shift_html_heading(self):
        # HTML Text to modify
        html_text = ''
        for level in range(1, 7):
            html_text += f'<h{level}>{level}</h{level}>\n'

        # Result expected
        offset = 2
        expected_html_text = ''
        for level in range(1, 7):
            new_level = level + offset
            if new_level > MAX_HTML_HEADING_LEVEL:
                expected_html_text += f'<div>{level}</div>\n'
            else:
                expected_html_text += f'<h{new_level}>{level}</h{new_level}>\n'

        self.assertEqual(
            shift_html_heading(html_text, offset),
            expected_html_text,
            msg='Shifted HTML text not corresponding to expted one',
        )


class ConvertTest(TestCase):
    """Test the convert functions"""
    def test_markdown_to_html(self):
        """Test convert.markdown_to_html()"""
        # Set markdown to convert
        markdown_test = '# Test title\nTest text'
        
        # Set expected html
        expected_html = '<h3 id="test-title">Test title</h3>\n<p>Test text</p>'

        # Check if Markdow to HTML have worked
        self.assertEqual(
            markdown_to_html(markdown_test),
            expected_html,
            msg='Markdown converted not corresponding to expted HTML',
        )


# Test models

class UserProfileModelTest(TestCase):
    """Test the model UserProfile"""

    def setUp(self):
        """Set up the test"""
        self.user_model = get_user_model()

        # Create a member, this will normally create a Profile
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Create a moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_record_edition(self):
        """Test that the record of an edition work"""
        # Create a new user
        created_user = self.user_model.objects.create_user(
            'temp_member',
            'temp_member@example.com',
            'temp_member_password',
        )

        # Get the profil of new user
        profile = created_user.profile

        # Check that the profile is not edited
        self.assertFalse(
            profile.edited,
            msg='Profile is already edited'
        )

        # Record an edition
        profile.record_edition(
            by_user=created_user,
            on_datetime=timezone.now(),
        )

        # Check the edition is recorded
        self.assertTrue(
            profile.edited,
            msg='Profile is not edited',
        )
        self.assertEqual(
            profile.edited_by,
            created_user,
            msg='Profile.edited_by is not correct',
        )
        self.assertTrue(
            profile.edited_on,
            msg='Profile.edited_on is not set',
        )
        
        # Delete the created user
        created_user.delete()
        
    def test_user_profile_created_for_member(self):
        """Test that a UserProfile has been created for the member"""
        self.assertNotEqual(
            getattr(self.member, 'profile'),
            None,
            msg='No profile created for member',
        )

    def test_latest_topic_set(self):
        """Test that .latest_topic_set return the created topics"""
        # Test that, for now, member have no Topic
        self.assertEqual(
            self.member.profile.latest_topic_set().count(),
            0,
            msg='Member get topics when it shouldn\'t',
        )
        
        # Create 2 topics for 3 days ago, from the member
        three_days_ago = timezone.now() - timedelta(days=3)
        created_topics = [
            Topic.objects.create(
                created_by=self.member,
                created_on=three_days_ago,
                category=self.test_category,
                subject=f'Subject {i}',
                message=f'Message {i}',
            )
            for i in range(2)
        ]

        # Try to get the 2 topics in a set
        self.assertEqual(
            self.member.profile.latest_topic_set().count(),
            2,
            msg='latest_topic_set() don\'t give 2 topics',
        )
        # Check it's the 2 created topics
        profile_latest_topic_set_pks = [
            topic.pk
            for topic in self.member.profile.latest_topic_set()
        ]
        created_topics_pks = [
            topic.pk
            for topic in created_topics
        ]
        self.assertTrue(
            all(
                [
                    topic_pk in created_topics_pks
                    for topic_pk in profile_latest_topic_set_pks
                ]
            ),
            msg='latest_topic_set() don\'t return the correct topics',
        )

        # Create 3 more topics for yesterday, from the member
        yesterday = timezone.now() - timedelta(days=1)
        created_topics += [
            Topic.objects.create(
                created_by=self.member,
                created_on=yesterday,
                category=self.test_category,
                subject=f'Subject {i+2}',
                message=f'Message {i+2}',
            )
            for i in range(3)
        ]

        # Try to get the 5 topics in a set
        self.assertEqual(
            self.member.profile.latest_topic_set().count(),
            5,
            msg='latest_topic_set() don\'t give 5 topics',
        )

        # Check we get all the created topics
        profile_latest_topic_set_pks = [
            topic.pk
            for topic in self.member.profile.latest_topic_set()
        ]
        created_topics_pks = [
            topic.pk
            for topic in created_topics
        ]
        self.assertTrue(
            all(
                [
                    topic_pk in created_topics_pks
                    for topic_pk in profile_latest_topic_set_pks
                ]
            ),
            msg='latest_topic_set() don\'t return the correct topics',
        )
        
        # Check the order
        self.assertGreater(
            profile_latest_topic_set_pks.index(
                created_topics_pks[1],
            ),
            profile_latest_topic_set_pks.index(
                created_topics_pks[4],
            ),
            msg='latest_topic_set() don\'t return topics in correct order',
        )

        # Create 3 more topics for the member
        created_topics += [
            Topic.objects.create(
                created_by=self.member,
                category=self.test_category,
                subject=f'Subject {i+5}',
                message=f'Message {i+5}',
            )
            for i in range(3)
        ]

        # Be sur the returned set is only 5
        self.assertEqual(
            self.member.profile.latest_topic_set().count(),
            5,
            msg='latest_topic_set() don\'t give 5 topics',
        )

        # Be sur the 2 first topics are not returned by .latest_topic_set()
        profile_latest_topic_set_pks = [
            topic.pk
            for topic in self.member.profile.latest_topic_set()
        ]
        created_topics_pks = [
            topic.pk
            for topic in created_topics[3:]
        ]
        self.assertTrue(
            created_topics[0].pk not in profile_latest_topic_set_pks,
            msg='First topic returned by latest_topic_set(), should not',
        )
        self.assertTrue(
            created_topics[1].pk not in profile_latest_topic_set_pks,
            msg='Second topic returned by latest_topic_set(), should not',
        )

        # Be sur the 5 return are the latest created Topic
        self.assertTrue(
            all(
                [
                    topic_pk in created_topics_pks
                    for topic_pk in profile_latest_topic_set_pks
                ]
            ),
            msg='latest_topic_set() don\'t return correct topics',
        )

        # Create 2 topic for another user
        moderator_created_topics = [
            Topic.objects.create(
                created_by=self.moderator,
                category=self.test_category,
                subject=f'Subject {i+8}',
                message=f'Message {i+8}',
            )
            for i in range(2)
        ]

        # Check we dont get themes by member profile
        profile_latest_topic_set_pks = [
            topic.pk
            for topic in self.member.profile.latest_topic_set()
        ]
        self.assertTrue(
            moderator_created_topics[0].pk not in profile_latest_topic_set_pks,
            msg='Moderatior topic returned by member profile latest_topic_set()',
        )
        self.assertTrue(
            moderator_created_topics[1].pk not in profile_latest_topic_set_pks,
            msg='Moderatior topic returned by member profile latest_topic_set()',
        )

        # Delete topic cerated from member
        [topic.delete() for topic in created_topics]

        # Check we still get no topics from another user
        self.assertEqual(
            self.member.profile.latest_topic_set().count(),
            0,
            msg='Get topic when should not',
        )

        # Delet topic for the other user
        [topic.delete() for topic in moderator_created_topics]

    def test_latest_topicreply_set(self):
        """Test that .latest_topicreply_set return the created topics"""
        # Create 1 topic from member, from 2 days ago
        two_days_ago = timezone.now() - timedelta(days=2)
        created_topic = Topic.objects.create(
            created_by=self.moderator,
            created_on=two_days_ago,
            category=self.test_category,
            subject='Subject 0',
            message='Message 0',
        )

        # Create 1 reply from moderator, from 2 days ago
        moderator_created_replies = [
            TopicReply.objects.create(
                created_by=self.moderator,
                created_on=two_days_ago,
                topic=created_topic,
                message='Reply from moderator',
            )
        ]

        # Create 1 reply from member, from 2 days ago
        member_created_replies = [
            TopicReply.objects.create(
                created_by=self.member,
                created_on=two_days_ago,
                topic=created_topic,
                message='Reply 0 from member',
            )
        ]

        # Get member reply with .latest_topicreply_set()
        member_latest_replies = self.member.profile.latest_topicreply_set()

        # Be sur we only got the member reply
        self.assertTrue(
            all(
                [
                    reply.created_by == self.member
                    for reply in member_latest_replies
                ]
            ),
            msg='Get replies not from the member',
        )
        self.assertEqual(
            member_latest_replies.count(),
            1,
            msg='Get more than 1 reply',
        )
        self.assertEqual(
            member_latest_replies[0].pk,
            member_created_replies[0].pk,
            msg='Reply get by latest_topicreply_set() not the one created',
        )

        # Create 5 more replies from member
        # 2 from yesterday
        yesterday = timezone.now() - timedelta(days=1)
        member_created_replies += [
            TopicReply.objects.create(
                created_by=self.member,
                created_on=yesterday,
                topic=created_topic,
                message=f'Reply {i+1} from member',
            )
            for i in range(2)
        ]

        # 3 from today
        member_created_replies += [
            TopicReply.objects.create(
                created_by=self.member,
                topic=created_topic,
                message=f'Reply {i+1} from member',
            )
            for i in range(3)
        ]

        # Get the 5 latest member relpies with .latest_topicreply_set()
        member_latest_replies = self.member.profile.latest_topicreply_set()

        # Be sure we get the latest member replies, and not the first
        self.assertEqual(
            member_latest_replies.count(),
            5,
            msg='latest_topicreply_set() do not return the 5 replies',
        )
        self.assertEqual(
            sorted(
                [
                    reply.pk
                    for reply in member_created_replies[1:]
                ],
                reverse=True,
            ),
            [
                reply.pk
                for reply in member_latest_replies
            ],
            msg='latest_topicreply_set() do not return correct replies',
        )

        # Delete the created topic
        created_topic.delete()

    def test_create_missing(self):
        """Test model class method .create_missing()"""
        # Remove the profile of the member
        old_profile_pk = self.member.profile.pk
        self.member.profile.delete()
        self.member = self.user_model.objects.get(
            pk=self.member.pk
        )

        # Check profile no more here
        self.assertFalse(
            UserProfile.objects.filter(pk=old_profile_pk).exists(),
            msg='Profile still exist',
        )

        # Get the user with missing profile
        users_with_no_profiles = self.user_model.objects.filter(
            profile__isnull=True
        )

        # Be sure we have only one
        self.assertEqual(
            users_with_no_profiles.count(),
            1,
            msg='Not found only 1 user without profile',
        )

        # And it's member
        self.assertEqual(
            users_with_no_profiles[0],
            self.member,
            msg='User without profile is not member',
        )

        # Run UserProfile.create_missing()
        UserProfile.create_missing(
            users_with_no_profiles
        )
        self.member = self.user_model.objects.get(
            pk=self.member.pk
        )

        # Check profile is here again
        self.assertNotEqual(
            self.member.profile,
            None,
            msg='Profile is not created by UserProfile.create_missing()',
        )
        

class TopicCategoryModelTest(TestCase):
    """Test the TopicCategory model"""
    def setUp(self):
        """Setup the test"""
        self.user_model = get_user_model()

        # Create a member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Create a moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )

    def test_topics_count(self):
        """Test property .topics_count()"""
        # Create a category
        created_category = TopicCategory.objects.create(
            title='One category',
            created_by=self.moderator,
        )

        # Create another category
        another_created_category = TopicCategory.objects.create(
            title='Another category',
            created_by=self.moderator,
        )

        # Create 3 topics linked to the first category
        for i in range(3):
            Topic.objects.create(
                subject=f'Test topic {i}',
                message=f'Test topic {i}',
                category=created_category,
                created_by=self.member,
            )

        # Create 1 topics linked to the second category
        Topic.objects.create(
            subject='Test topic',
            message='Test topic',
            category=another_created_category,
            created_by=self.member,
        )

        # Get .topics_count and test it return 3
        self.assertEqual(
            created_category.topics_count,
            3,
            msg='Do not get 3 from topics_count',
        )

        # Delete the 2 categories
        created_category.delete()
        another_created_category.delete()

    def test_replies_count(self):
        """Test the property .replies_count"""
        # Create a category
        created_category = TopicCategory.objects.create(
            title='One category',
            created_by=self.moderator,
        )

        # Create another category
        another_created_category = TopicCategory.objects.create(
            title='Another category',
            created_by=self.moderator,
        )

        # Create 2 topics in first category and 1 in the second
        first_topic = Topic.objects.create(
            subject='Test topic 1',
            message='Test topic 1',
            category=created_category,
            created_by=self.member,
        )
        second_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=created_category,
            created_by=self.member,
        )
        third_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=another_created_category,
            created_by=self.member,
        )

        # Create 2 replies in the first topic in first category
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 1',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 2',
            created_by=self.moderator,
        )

        # Create 3 replies in the second topic in first category
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 3',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 4',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 5',
            created_by=self.moderator,
        )

        # Create 2 replies in the first topic in second category
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 4',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 5',
            created_by=self.moderator,
        )

        # get .replies_count and check we got 5
        self.assertEqual(
            created_category.replies_count,
            5,
            msg='Do not get 5 replies with .replies_count'
        )
        
        # Delete the 2 categories
        created_category.delete()
        another_created_category.delete()

    def test_messages_count(self):
        """Test the property .messages_count"""
        # Create a category
        created_category = TopicCategory.objects.create(
            title='One category',
            created_by=self.moderator,
        )

        # Create another category
        another_created_category = TopicCategory.objects.create(
            title='Another category',
            created_by=self.moderator,
        )

        # Create 2 topics in first category and 1 in the second
        first_topic = Topic.objects.create(
            subject='Test topic 1',
            message='Test topic 1',
            category=created_category,
            created_by=self.member,
        )
        second_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=created_category,
            created_by=self.member,
        )
        third_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=another_created_category,
            created_by=self.member,
        )

        # Create 2 replies in the first topic in first category
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 1',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 2',
            created_by=self.moderator,
        )

        # Create 3 replies in the second topic in first category
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 3',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 4',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 5',
            created_by=self.moderator,
        )

        # Create 2 replies in the first topic in second category
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 4',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 5',
            created_by=self.moderator,
        )

        # get .messages_count and check we got 7
        self.assertEqual(
            created_category.messages_count,
            7,
            msg='Do not get 7 messages with .replies_count'
        )
        
        # Delete the 2 categories
        created_category.delete()
        another_created_category.delete()

    def test_latest_topic(self):
        """Test the property .latest_topic"""
        # Create a category
        created_category = TopicCategory.objects.create(
            title='One category',
            created_by=self.moderator,
        )

        # Create another category
        another_created_category = TopicCategory.objects.create(
            title='Another category',
            created_by=self.moderator,
        )

        # Create 1 topic from yesterday in first category
        yesterday_topic = Topic.objects.create(
            subject='Test topic from yesterday',
            message='Test topic from yesterday',
            category=created_category,
            created_on=timezone.now()-timedelta(days=1),
            created_by=self.member,
        )
        
        # Create 1 topic from today in first category
        today_topic = Topic.objects.create(
            subject='Test topic from yesterday',
            message='Test topic from yesterday',
            category=created_category,
            created_on=timezone.now(),
            created_by=self.member,
        )

        # Create 1 topics linked to the second category
        Topic.objects.create(
            subject='Test topic',
            message='Test topic',
            category=another_created_category,
            created_by=self.member,
        )

        # Get .latest_topic and test it return today_topic
        self.assertEqual(
            created_category.latest_topic,
            today_topic,
            msg='Do not get latest topic',
        )

        # Delete the 2 categories
        created_category.delete()
        another_created_category.delete()

    def test_latest_reply(self):
        """Test the property .latest_reply"""
        # Create a category
        created_category = TopicCategory.objects.create(
            title='One category',
            created_by=self.moderator,
        )

        # Create another category
        another_created_category = TopicCategory.objects.create(
            title='Another category',
            created_by=self.moderator,
        )

        # Create 2 topics in first category and 1 in the second
        first_topic = Topic.objects.create(
            subject='Test topic 1',
            message='Test topic 1',
            category=created_category,
            created_by=self.member,
        )
        second_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=created_category,
            created_by=self.member,
        )
        third_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=another_created_category,
            created_by=self.member,
        )

        # Create 2 replies from 2 days ago in the first topic in first category
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 1',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 2',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )

        # Create 2 replies from yesterday in the second topic in first category
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 3',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 4',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )
        
        # Create 2 replies from today in the second topic in first category
        latest_reply = TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 5',
            created_on=timezone.now(),
            created_by=self.moderator,
        )

        # Create 2 replies in the first topic in second category
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 4',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 5',
            created_by=self.moderator,
        )

        # get .latest_reply and check we got the latest reply
        self.assertEqual(
            created_category.latest_reply,
            latest_reply,
            msg='Do not get the latest reply'
        )
        
        # Delete the 2 categories
        created_category.delete()
        another_created_category.delete()
        
    def test_latest_message_created_on(self):
        """Test the property .latest_message_created_on"""
        # Create a category
        created_category = TopicCategory.objects.create(
            title='One category',
            created_by=self.moderator,
        )

        # Create another category
        another_created_category = TopicCategory.objects.create(
            title='Another category',
            created_by=self.moderator,
        )

        # Create 2 topics in first category and 1 in the second, 10
        # days ago
        ten_days_ago = timezone.now() - timedelta(days=10)
        first_topic = Topic.objects.create(
            subject='Test topic 1',
            message='Test topic 1',
            category=created_category,
            created_by=self.member,
            created_on=ten_days_ago,
        )
        second_topic = Topic.objects.create(
            subject='Test topic 2',
            message='Test topic 2',
            category=created_category,
            created_by=self.member,
            created_on=ten_days_ago,
        )
        third_topic = Topic.objects.create(
            subject='Test topic 3',
            message='Test topic 3',
            category=another_created_category,
            created_by=self.member,
            created_on=ten_days_ago,
        )

        # get .latest_message_created_on and check we got correct
        # datetime
        self.assertEqual(
            created_category.latest_message_created_on,
            ten_days_ago,
            msg='Do not get the correct latest message created_on'
        )

        # Create 2 replies from 2 days ago in the first topic in first category
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 1',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=first_topic,
            message='Test reply 2',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )

        # Create 2 replies from yesterday in the second topic in first category
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 3',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 4',
            created_on=timezone.now()-timedelta(days=2),
            created_by=self.moderator,
        )
        
        # Create 2 replies from today in the second topic in first category
        today = timezone.now()
        latest_reply = TopicReply.objects.create(
            topic=second_topic,
            message='Test reply 5',
            created_on=today,
            created_by=self.moderator,
        )

        # Create 2 replies in the first topic in second category
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 6',
            created_by=self.moderator,
        )
        TopicReply.objects.create(
            topic=third_topic,
            message='Test reply 7',
            created_by=self.moderator,
        )

        # get .latest_message_created_on and check we got correct
        # datetime
        self.assertEqual(
            created_category.latest_message_created_on,
            today,
            msg='Do not get the correct latest message created_on'
        )
        
        # Delete the 2 categories
        created_category.delete()
        another_created_category.delete()
        

class TopicModelTest(TestCase):
    """Test the Topic model"""
    def setUp(self):
        """Setup the test"""
        self.user_model = get_user_model()

        # Create a member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Create a moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_record_edition(self):
        """Test that the record of an edition work"""
        # Create a new topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject='Test subject',
            message='Test message',
        )

        # Check that the topic is not edited
        self.assertFalse(
            created_topic.edited,
            msg='Topic is already edited'
        )

        # Record an edition
        created_topic.record_edition(
            by_user=self.member,
            on_datetime=timezone.now(),
        )

        # Check the edition is recorded
        self.assertTrue(
            created_topic.edited,
            msg='Topic is not edited',
        )
        self.assertEqual(
            created_topic.edited_by,
            self.member,
            msg='Topic.created_by is not correct'
        )
        self.assertTrue(
            created_topic.edited_on,
            msg='Topic.created_on is not set'
        )
        
        # Delete the created user
        created_topic.delete()
        
    def test_replies_count(self):
        """Test the property .replies_count"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Create 2 replies
        TopicReply.objects.create(
            created_by=self.member,
            topic=created_topic,
            message="Test reply 1",
        )
        TopicReply.objects.create(
            created_by=self.member,
            topic=created_topic,
            message="Test reply 2",
        )

        # Test that .replies_count return 2
        self.assertEqual(
            created_topic.replies_count,
            2,
        )

        # Remove created topic, and replies
        created_topic.delete()

    def test_latest_reply(self):
        """Test the property latest reply"""
        yesterday = timezone.now() - timedelta(days=1)
        
        # Create a topic for yesterday
        created_topic = Topic.objects.create(
            created_by=self.member,
            created_on=yesterday,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Create 2 replies, one yesterday
        created_replies = []
        created_replies.append(
            TopicReply.objects.create(
                created_by=self.member,
                created_on=yesterday,
                topic=created_topic,
                message="Test reply 1",
            )
        )
        created_replies.append(
            TopicReply.objects.create(
                created_by=self.member,
                topic=created_topic,
                message="Test reply 2",
            )
        )

        # Tast that .latest_reply return the latest created reply
        self.assertEqual(
            created_topic.latest_reply.pk,
            created_replies[1].pk,
        )

        # Delete the created topic and replies
        created_topic.delete()

    def test_last_message_on_default(self):
        """Test that last_message_on is set at Topic creation"""
        # Create a new topic
        now = timezone.now()
        created_topic = Topic.objects.create(
            created_by=self.member,
            created_on=now,
            subject='Test',
            category=self.test_category,
            message='Test',
        )

        # Test last_message_on is set to created_on
        self.assertEqual(
            created_topic.last_message_on,
            now,
            msg='Topic last_message_on is no set correctly',
        )

        # Delete created topic
        created_topic.delete()


class TopicReplyModelTest(TestCase):
    """All test for the mode TopicReply"""
    def setUp(self):
        """Setup the test"""
        self.user_model = get_user_model()

        # Create a member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Create a moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_record_edition(self):
        """Test that the record of an edition work"""
        # Create a new topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject='Test subject',
            message='Test message',
        )

        # Create a new reply
        created_topicreply = TopicReply.objects.create(
            created_by=self.moderator,
            topic=created_topic,
            message='Test reply',
        )

        # Check that the topic reply is not edited
        self.assertFalse(
            created_topicreply.edited,
            msg='Topic reply is already edited'
        )

        # Record an edition
        created_topicreply.record_edition(
            by_user=self.moderator,
            on_datetime=timezone.now(),
        )

        # Check the edition is recorded
        self.assertTrue(
            created_topicreply.edited,
            msg='Topic is not edited',
        )
        self.assertEqual(
            created_topicreply.edited_by,
            self.moderator,
            msg='Topic reply .created_by is not correct'
        )
        self.assertTrue(
            created_topicreply.edited_on,
            msg='Topic reply .created_on is not set'
        )
        
        # Delete the created user
        created_topic.delete()

    def test_topic_update_last_message_on(self):
        """Test that topic update its last_message_on when reply"""
        # Create a new topic
        yesterday = timezone.now() - timedelta(days=1)
        created_topic = Topic.objects.create(
            created_by=self.member,
            created_on=yesterday,
            subject='Test',
            category=self.test_category,
            message='Test',
        )

        # Create a reply
        now = timezone.now()
        created_reply = TopicReply.objects.create(
            created_by=self.member,
            created_on=now,
            topic=created_topic,
            message='Test',
        )

        # Test last_message_on is set to created_on
        self.assertEqual(
            created_topic.last_message_on,
            now,
            msg='Topic last_message_on is no set correctly',
        )

        # Delete created topic
        created_topic.delete()        


class BannerModelTest(TestCase):
    """Test the model Banner"""

    def test_get_currently_displayed(self):
        """Test the class method .get_currently_displayed()"""
        # Create an expired banner
        banner_expired = Banner.objects.create(
            title='Expired banner',
            message='Expired banner',
            display_from=timezone.now()-timedelta(days=10),
            display_to=timezone.now()-timedelta(days=5),
        )

        # Create an banner who expire now
        banner_expired_now = Banner.objects.create(
            title='Expired now banner',
            message='Expired banner',
            display_from=timezone.now()-timedelta(days=10),
            display_to=timezone.now(),
        )

        # Create a banner to be displayed now, without expiration date
        banner_present = Banner.objects.create(
            title='Present banner',
            message='Present banner',
        )

        # Create a banner to be displayed now, with expiration date in
        # the future
        banner_with_future_expiration = Banner.objects.create(
            title='Present banner with future expiration',
            message='Present banner with future expiration',
            display_to=timezone.now()+timedelta(days=1),
        )

        # Create a banner to be displayed in the future
        banner_future = Banner.objects.create(
            title='Future banner',
            message='Future banner',
            display_from=timezone.now()+timedelta(days=10),
            display_to=timezone.now()+timedelta(days=15),
        )

        # Call .get_currently_displayed()
        currently_displayed_banners = Banner.get_currently_displayed()

        # Test we only get the two present banners
        self.assertEqual(
            currently_displayed_banners.count(),
            2,
            msg='Do not get only 2 banners',
        )
        present_banners_pks = [
            banner_present.pk,
            banner_with_future_expiration.pk,
        ]
        self.assertEqual(
            sorted(
                present_banners_pks
            ),
            sorted(
                currently_displayed_banners.values_list(
                    'pk',
                    flat=True,
                )
            ),
            msg='Do not get the currently displayed banners'
        )

        # Delete the banners
        Banner.objects.all().delete()

    def test_delete_expired(self):
        """test the class method .delete_expired()"""
        # Delete all the remaining banners
        Banner.objects.all().delete()
        
        # Create an expired banner
        banner_expired = Banner.objects.create(
            title='Expired banner',
            message='Expired banner',
            display_from=timezone.now()-timedelta(days=10),
            display_to=timezone.now()-timedelta(days=5),
        )

        # Create an banner who expire now
        banner_expired_now = Banner.objects.create(
            title='Expired now banner',
            message='Expired banner',
            display_from=timezone.now()-timedelta(days=10),
            display_to=timezone.now(),
        )

        # Create a banner to be displayed now, without expiration date
        banner_present = Banner.objects.create(
            title='Present banner',
            message='Present banner',
        )

        # Create a banner to be displayed now, with expiration date in
        # the future
        banner_with_future_expiration = Banner.objects.create(
            title='Present banner with future expiration',
            message='Present banner with future expiration',
            display_to=timezone.now()+timedelta(days=1),
        )

        # Call .delete_expired()
        Banner.delete_expired()

        # Test we still have only 2 banner
        self.assertEqual(
            Banner.objects.all().count(),
            2,
            msg='Do not have only one banner',
        )

        # Test it is the presente and future banner
        banner_keeped_pk = [
            banner_present.pk,
            banner_with_future_expiration.pk,
        ]
        self.assertEqual(
            sorted(
                Banner.objects.all().values_list(
                    'pk',
                    flat=True,
                ),
            ),
            sorted(
                banner_keeped_pk,
            ),
            msg='Banners still here not corresponding to what planned'
        )

        # Delete the banners
        Banner.objects.all().delete()

    def test_delete_expired_limited(self):
        """test the class method .delete_expired(), limited to a queryset"""
        # Delete all the remaining banners
        Banner.objects.all().delete()
        
        # Create an expired banner
        banner_expired = Banner.objects.create(
            title='Expired banner',
            message='Expired banner',
            display_from=timezone.now()-timedelta(days=10),
            display_to=timezone.now()-timedelta(days=5),
        )

        # Create an banner who expire now
        banner_expired_now = Banner.objects.create(
            title='Expired now banner',
            message='Expired banner',
            display_from=timezone.now()-timedelta(days=10),
            display_to=timezone.now(),
        )

        # Create a banner to be displayed now, without expiration date
        banner_present = Banner.objects.create(
            title='Present banner',
            message='Present banner',
        )

        # Create a banner to be displayed now, with expiration date in
        # the future
        banner_with_future_expiration = Banner.objects.create(
            title='Present banner with future expiration',
            message='Present banner with future expiration',
            display_to=timezone.now()+timedelta(days=1),
        )

        # Call .delete_expired(), but limited to a queryset who
        # excluded banner_expired
        Banner.delete_expired(
            queryset=Banner.objects.exclude(
                pk=banner_expired.pk,
            )
        )

        # Test we still have only 3 banner
        self.assertEqual(
            Banner.objects.all().count(),
            3,
            msg='Do not have only one banner',
        )

        # Test it is the presente and future banner
        banner_keeped_pk = [
            banner_expired.pk,
            banner_present.pk,
            banner_with_future_expiration.pk,
        ]
        self.assertEqual(
            sorted(
                Banner.objects.all().values_list(
                    'pk',
                    flat=True,
                ),
            ),
            sorted(
                banner_keeped_pk,
            ),
            msg='Banners still here not corresponding to what planned'
        )

        # Delete the banners
        Banner.objects.all().delete()


# Test views

class TopicCreateViewTest(TestCase):
    """Test the view TopicCreateView"""

    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')

        # Create a member group
        self.member_group = Group.objects.create(name='Members')
        self.member_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'add_topic',
                'slow_forum',
                'topic',
            ),
        )
        self.member_group.save()
        
        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member',
            'member@example.com',
            'member_password',
        )
        self.member.groups.add(
            self.member_group,
        )
        self.member.save()

        # Banned member
        self.banned_member = self.user_model.objects.create_user(
            'banned_member',
            'banned_member@example.com',
            'banned_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the acces to TopicCreateView"""
        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicCreateView,
            reverse(
                'slow_forum:topic-add',
            ),
        )
        
        # Test access with banned user and alloew user
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-add',
            ),
            self.banned_member,
            self.member,
        )

    def test_created_by_field_set(self):
        """Test, when Topic is created, that cerated_by field is set"""
        # Connect as member
        self.client.logout()
        self.client.force_login(
            user=self.member,
        )

        # Be sure there is no topics
        Topic.objects.all().delete()

        # Create a topic by a POST request
        self.client.post(
            reverse(
                'slow_forum:topic-add',
            ),
            {
                'category': self.test_category.pk,
                'subject': 'This is a test subject',
                'message': 'This is a test message',
            }
        )

        # Get the created topic
        created_topic = Topic.objects.get(subject='This is a test subject')

        # Test its field "created_by" is set to member
        self.assertEqual(
            created_topic.created_by,
            self.member,
        )

        # Delete the created topic
        created_topic.delete()

    def test_get_context_data(self):
        """Test that, when HTTP request, context is correctly built"""
        # Connect as member
        self.client.logout()
        self.client.force_login(
            user=self.member,
        )

        # Do a GET request on view URL without category
        response = self.client.get(
            reverse(
                'slow_forum:topic-add'
            ),
        )

        # Check category is not in the context
        self.assertEqual(
            response.context.get('category', None),
            None,
        )

        # Do a GET request on view URL with category
        response = self.client.get(
            reverse(
                'slow_forum:topic-add-in-category',
                kwargs={
                    'topic_category_pk': self.test_category.pk,
                },
            ),
        )

        # Check category is in the context
        self.assertEqual(
            response.context.get('category'),
            self.test_category,
        )

    def test_get_form_kwargs(self):
        """Test when GET request the form is made with correct default data"""
        # Connect as memeber
        self.client.logout()
        self.client.force_login(
            user=self.member,
        )

        # Do a GET request on view URL without category
        response = self.client.get(
            reverse(
                'slow_forum:topic-add'
            ),
        )

        # Check the form initial data is not set for category
        self.assertEqual(
            response.context.get('form').initial.get('category'),
            None,
        )

        # Do a GET request on view URL with category
        response = self.client.get(
            reverse(
                'slow_forum:topic-add-in-category',
                kwargs={
                    'topic_category_pk': self.test_category.pk,
                },
            ),
        )

        # Check the form initial data is set for category
        self.assertEqual(
            response.context.get('form').initial.get('category'),
            self.test_category.pk,
        )


class TopicUpdateViewTest(TestCase):
    """Test of the view Topic"""

    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'change_topic',
                'slow_forum',
                'topic',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the view access to TopicUpdateView"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicUpdateView,
            reverse(
                'slow_forum:topic-update',
                kwargs={
                    'pk': created_topic.pk,
                },
            ),
        )

        # Test if a member can access TopicUpdateView of its own
        # topic, and another user can't
        test_access_owner(
            self,
            reverse(
                'slow_forum:topic-update',
                kwargs={
                    'pk': created_topic.pk,
                },
            ),
            self.other_member,
            self.member,
        )

        # Test if a moderation can access TopicUpdateView for a member
        # topic, based on its permission. And test that a user not
        # owner of the topic and without the right permission can't
        # access it.
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-update',
                kwargs={
                    'pk': created_topic.pk,
                },
            ),
            self.other_member,
            self.moderator,
        )

        # Remove created topic
        created_topic.delete()

    def test_edition_record(self):
        """Test edition is recorded

        Test that, after topic edition:
        * edited field is set at true
        * edited_by field user is the same as the user who edited the topic
        * edited_on field is set
        """
        # Create a new topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject='Test subject',
            message='Test message',
        )

        test_object_edition_record_with_view(
            self,
            created_topic,
            self.member,
            reverse(
                'slow_forum:topic-update',
                kwargs={
                    'pk': created_topic.pk,
                },
            ),
            {
                'subject': 'New test subject',
                'message': 'New test message',
                'category': self.test_category.pk,
            },
            302,
        )

        # Check that the sent data are recorder on the profil
        self.assertEqual(
            created_topic.subject,
            'New test subject',
            msg='New subject is not set correctly',
        )
        self.assertEqual(
            created_topic.message,
            'New test message',
            msg='New message is not set correctly',
        )

        # Delete the created user
        created_topic.delete()


class TopicDeleteViewTest(TestCase):
    """Test the view TopicDeletView"""
    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'delete_topic',
                'slow_forum',
                'topic',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the access to view TopicUpdateView"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicDeleteView,
            reverse(
                'slow_forum:topic-delete',
                kwargs={
                    'pk': created_topic.pk,
                },
            ),
        )

        # Test if a moderation can access TopicDeleteView for a member
        # topic, based on its permission. And test that a user without
        # the right permission can't access it.
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-delete',
                kwargs={
                    'pk': created_topic.pk,
                },
            ),
            self.member,
            self.moderator,
        )

        # Remove created topic
        created_topic.delete()
        

class TopicReplyCreateViewTest(TestCase):
    """Test the view TopicReplyCreateView"""

    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')

        # Create a member group
        self.member_group = Group.objects.create(name='Members')
        self.member_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'add_topicreply',
                'slow_forum',
                'topicreply',
            ),
        )
        self.member_group.save()
        
        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member',
            'member@example.com',
            'member_password',
        )
        self.member.groups.add(
            self.member_group,
        )
        self.member.save()

        # Banned member
        self.banned_member = self.user_model.objects.create_user(
            'banned_member',
            'banned_member@example.com',
            'banned_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the acces to TopicReplyCreateView"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )
        
        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicReplyCreateView,
            reverse(
                'slow_forum:topic-reply-add',
                kwargs={
                    'topic_pk': created_topic.pk,
                },
            ),
        )
        
        # Test access with banned user and alloew user
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-reply-add',
                kwargs={
                    'topic_pk': created_topic.pk,
                },
            ),
            self.banned_member,
            self.member,
        )

        # Remove created topic
        created_topic.delete()

    def test_404(self):
        """Test we have 404 if get or post method for a non existant Topic"""
        # Log as member, to have the right to reply
        self.client.logout()
        self.client.force_login(
            user=self.member,
        )

        # Do a get request on TopicReplyCreateView, for a non existant Topic
        response = self.client.get(
            reverse(
                'slow_forum:topic-reply-add',
                kwargs={
                    'topic_pk': 9999,
                },
            ),
        )

        # Check we get an 404
        self.assertEqual(
            response.status_code,
            404,
            msg='Get request to reply on a non existant Topic do not get 404',
        )

        # Do a get request on TopicReplyCreateView, for a non existant Topic
        response = self.client.post(
            reverse(
                'slow_forum:topic-reply-add',
                kwargs={
                    'topic_pk': 9999,
                },
            ),
            {
                'message': 'Message for a test',
            }
        )

        # Check we get an 404
        self.assertEqual(
            response.status_code,
            404,
            msg='Post request to reply on a non existant Topic do not get 404',
        )
        
    def test_topic_reply_creation(self):
        """Test the creation of a reply

        Test done:
        * Check the created reply have the topic field set
        * Check the created reply have the created_by field set
        """
        # Create a topic from moderator
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Do a reply with member
        self.client.logout()
        self.client.force_login(
            user=self.member,
        )

        response = self.client.post(
            reverse(
                'slow_forum:topic-reply-add',
                kwargs={
                    'topic_pk': created_topic.pk,
                },
            ),
            {
                'message': 'Message for a test',
            }
        )

        # Check the reply model instance is created

        # Get the reply through the topic proves that the "topic"
        # field of the reply is correctly set
        self.assertEqual(
            created_topic.replies_count,
            1,
        )
        reply = created_topic.replies_set.all()[0]

        # Check that the reply have a created_by correctly set to
        # member
        self.assertEqual(
            reply.created_by,
            self.member,
        )

        # Delete the created topic
        created_topic.delete()


class TopicReplyUpdateViewTest(TestCase):
    """Test the view TopicReplyUpdateView"""

    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'change_topicreply',
                'slow_forum',
                'topicreply',
            )
        )
        self.moderators_group.save()

        # Create a member group
        self.member_group = Group.objects.create(name='Members')
        self.member_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'add_topicreply',
                'slow_forum',
                'topicreply',
            ),
        )
        self.member_group.save()
        
        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member',
            'member@example.com',
            'member_password',
        )
        self.member.groups.add(
            self.member_group,
        )
        self.member.save()

        # Banned member
        self.banned_member = self.user_model.objects.create_user(
            'banned_member',
            'banned_member@example.com',
            'banned_member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the view access to TopicUpdateView"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Ceate a reply
        created_reply = TopicReply.objects.create(
            created_by=self.member,
            topic=created_topic,
            message='Test reply',
        )

        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicReplyUpdateView,
            reverse(
                'slow_forum:topic-reply-update',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
        )

        # Test if a member can access TopicReplyUpdateView for its own
        # topic, and another user can't
        test_access_owner(
            self,
            reverse(
                'slow_forum:topic-reply-update',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
            self.other_member,
            self.member,
        )

        # Test if a moderation can access TopicReplyUpdateView for a
        # member topic reply, based on its permission. And test that a
        # user not owner of the topic reply and without the right
        # permission can't access it.
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-reply-update',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
            self.other_member,
            self.moderator,
        )

        # Remove created topic
        created_topic.delete()

    def test_edition_record(self):
        """Test edition is recorded

        Test that, after a topic reply edition:
        * edited field is set at true
        * edited_by field user is the same as the user who edited the reply
        * edited_on field is set
        """
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.moderator,
            category=self.test_category,
            subject='Test topic',
            message='Test message',
        )

        # Ceate a reply
        created_reply = TopicReply.objects.create(
            created_by=self.member,
            topic=created_topic,
            message='Test reply',
        )

        test_object_edition_record_with_view(
            self,
            created_reply,
            self.member,
            reverse(
                'slow_forum:topic-reply-update',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
            {
                'message': 'New test reply',
            },
            302,
        )

        # Check that the sent data are recorder on the reply
        self.assertEqual(
            created_reply.message,
            'New test reply',
            msg='New message is not set correctly',
        )

        # Delete the created user
        created_topic.delete()


class TopicReplyDeleteViewTest(TestCase):
    """Test the view TopicReplyDeletView"""
    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'delete_topicreply',
                'slow_forum',
                'topicreply',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the view access to TopicReplyUpdateView"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Create a topic reply
        created_reply = TopicReply.objects.create(
            created_by=self.member,
            topic=created_topic,
            message="Test reply",
        )

        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicReplyDeleteView,
            reverse(
                'slow_forum:topic-reply-delete',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
        )

        # Test if a moderation can access TopicReplyDeleteView for a
        # member topic reply, based on its permission. And test that a
        # user without the right permission can't access it.
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-reply-delete',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
            self.member,
            self.moderator,
        )
        
        # Remove created topic
        created_topic.delete()

    def test_success_url(self):
        """Test the correct URl is get in case on success"""
        # Create a topic
        created_topic = Topic.objects.create(
            created_by=self.member,
            category=self.test_category,
            subject="Test subject",
            message="Test message",
        )

        # Create a topic reply
        created_reply = TopicReply.objects.create(
            created_by=self.member,
            topic=created_topic,
            message="Test reply",
        )

        # Connect as the moderation
        self.client.logout()
        self.client.force_login(
            user=self.moderator,
        )

        # Try to delete the reply with a post request on
        # TopicReplyDeleteView
        response = self.client.post(
            reverse(
                'slow_forum:topic-reply-delete',
                kwargs={
                    'topic_pk': created_topic.pk,
                    'pk': created_reply.pk,
                },
            ),
            {},
        )

        # Test if we have been redirected to the url of the created
        # topic
        self.assertEqual(
            response.status_code,
            302,
            msg='Not redirected after the post request',
        )
        self.assertEqual(
            response.url,
            created_topic.get_absolute_url(),
            msg='Redirect url is not correct',
        )
        
        # Remove created topic
        created_topic.delete()


class TopicCategoryCreateViewTest(TestCase):
    """Test the view TopicCategoryCreateView"""

    def setUp(self):
        """Setup for the test"""
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'add_topiccategory',
                'slow_forum',
                'topiccategory',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

    def test_access(self):
        """Test the access limitation of the view"""
        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicCategoryCreateView,
            reverse(
                'slow_forum:topic-category-add',
            ),
        )
        
        # Test access with member (not alowed to create a category)
        # and a moderator (allowed to create a category)
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-category-add',
            ),
            self.member,
            self.moderator,
        )
        
    def test_created_by_field_set(self):
        """Test that the created_by field is set when a category is created"""
        # Connect as moderator
        self.client.logout()
        self.client.force_login(
            user=self.moderator,
        )

        # Be sure there is no category
        TopicCategory.objects.all().delete()

        # Create a topic by a POST request
        self.client.post(
            reverse(
                'slow_forum:topic-category-add',
            ),
            {
                'title': 'This is a test category',
                'priority': 0,
                'short_description': 'This is a test short description',
                'full_description': 'This is a test full description',
            }
        )

        # Get the created category
        created_category = TopicCategory.objects.get(title='This is a test category')

        # Test its field "created_by" is set to moderator
        self.assertEqual(
            created_category.created_by,
            self.moderator,
        )

        # Delete the created topic
        created_category.delete()


class TopicCategoryUpdateViewTest(TestCase):
    """Test the view TopicCategoryUpdateView"""
    def setUp(self):
        """Setup for the test"""
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'change_topiccategory',
                'slow_forum',
                'topiccategory',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

    def test_access(self):
        """Test the access limitation of the view"""
        # Create a new topic category
        created_category = TopicCategory.objects.create(
            created_by=self.member,
            title='Test category',
            short_description='Test category',
            full_description='Test category',
        )

        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicCategoryUpdateView,
            reverse(
                'slow_forum:topic-category-update',
                kwargs={
                    'pk': created_category.pk,
                },
            ),
        )
        
        # Test access with member (not alowed to create a category)
        # and a moderator (allowed to create a category)
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-category-update',
                kwargs={
                    'pk': created_category.pk,
                },
            ),
            self.member,
            self.moderator,
        )

        # Delete the created user
        created_category.delete()

    def test_record_edition(self):
        # Create a new topic category
        created_category = TopicCategory.objects.create(
            created_by=self.member,
            title='Test category',
            short_description='Test category',
            full_description='Test category',
        )

        # Test edition with the view record edition info
        test_object_edition_record_with_view(
            self,
            created_category,
            self.moderator,
            reverse(
                'slow_forum:topic-category-update',
                kwargs={
                    'pk': created_category.pk,
                },
            ),
            {
                'title': 'New test category',
                'priority': 4,
                'short_description': 'New test category',
                'full_description': 'New test category',
            },
            302,
        )
        
        # Delete the created user
        created_category.delete()


class TopicCategoryDeleteViewTest(TestCase):
    """Test the view TopicCategoryDeleteView"""
    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'delete_topiccategory',
                'slow_forum',
                'topiccategory',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the access to view TopicCategoryDeleteView"""
        # Create a topic category
        created_category = TopicCategory.objects.create(
            created_by=self.moderator,
            title='Test category title',
            priority=0,
        )

        # Test access with not logged user
        test_access_logging_required(
            self,
            TopicCategoryDeleteView,
            reverse(
                'slow_forum:topic-category-delete',
                kwargs={
                    'pk': created_category.pk,
                },
            ),
        )

        # Test if a moderation can access TopicCategoryDeleteView for
        # a category, based on its permission. And test that a
        # user without the right permission can't access it.
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:topic-category-delete',
                kwargs={
                    'pk': created_category.pk,
                },
            ),
            self.member,
            self.moderator,
        )

        # Remove created topic category
        created_category.delete()

class UserProfileDetailViewTest(TestCase):
    """Test the view UserProfileDetailView"""
    
    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'change_userprofile',
                'slow_forum',
                'userprofile',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_context_buld_tab_infos(self):
        """Test that the tab infos are correctly add into the context"""
        # Do a request into the user profile default view
        self.client.logout()
        response = self.client.get(
            reverse(
                'slow_forum:user-profile-detail',
                kwargs={
                    'pk': self.member.profile.pk,
                },
            ),
        )

        # Test the tab name, in context, is "general"
        self.assertEqual(
            response.context.get('tab_name'),
            'general',
            msg='Tab name is not "general"',
        )

        # Do a request into the user profile tab view, ask for general
        # infos tab
        response = self.client.get(
            reverse(
                'slow_forum:user-profile-detail-tab',
                kwargs={
                    'pk': self.member.profile.pk,
                    'tab_name': 'general',
                },
            ),
        )

        # Test the tab name, in context, is "general"
        self.assertEqual(
            response.context.get('tab_name'),
            'general',
            msg='Tab name is not "general"',
        )

        # Do a request into the user profile tab view, ask for topic
        # list
        response = self.client.get(
            reverse(
                'slow_forum:user-profile-detail-tab',
                kwargs={
                    'pk': self.member.profile.pk,
                    'tab_name': 'topics_list',
                },
            ),
        )

        # Test the tab name, in context, is "topics_list"
        self.assertEqual(
            response.context.get('tab_name'),
            'topics_list',
            msg='Tab name is not "topics_list"',
        )

        # Test the context data "topics_list" is present
        self.assertTrue(
            'topics_list' in response.context.keys(),
            msg='"topics_list" data is not in context',
        )

        # Do a request into the user profile tab view, ask for topic
        # replies list
        response = self.client.get(
            reverse(
                'slow_forum:user-profile-detail-tab',
                kwargs={
                    'pk': self.member.profile.pk,
                    'tab_name': 'topics_replies_list',
                },
            ),
        )

        # Test the tab name, in context, is "topics_replies_list"
        self.assertEqual(
            response.context.get('tab_name'),
            'topics_replies_list',
            msg='Tab name is not "topics_replies_list"',
        )
        
        # Test the context data "topics_replies_list" is present
        self.assertTrue(
            'topics_replies_list' in response.context.keys(),
            msg='"topics_replies_list" data is not in context',
        )

        # Do a request into the user profile tab view, ask for a tab
        # non existant
        response = self.client.get(
            reverse(
                'slow_forum:user-profile-detail-tab',
                kwargs={
                    'pk': self.member.profile.pk,
                    'tab_name': 'food_list',
                },
            ),
        )

        # Test the tab name, in context, is "general"
        self.assertEqual(
            response.context.get('tab_name'),
            'general',
            msg='Tab name is not "general"',
        )


class UserProfileUpdateViewTest(TestCase):
    """Test the view UserProfileUpdateView"""
    
    def setUp(self):
        self.client = Client()
        self.user_model = get_user_model()
        
        # Create a moderators group
        self.moderators_group = Group.objects.create(name='Moderators')
        self.moderators_group.permissions.add(
            Permission.objects.get_by_natural_key(
                'change_userprofile',
                'slow_forum',
                'userprofile',
            ),
        )
        self.moderators_group.save()

        # Create the users
        # Moderator
        self.moderator = self.user_model.objects.create_user(
            'moderator', 'moderator@example.com', 'moderator_password',
        )
        self.moderator.groups.add(
            self.moderators_group,
        )
        self.moderator.save()

        # Member
        self.member = self.user_model.objects.create_user(
            'member', 'member@example.com', 'member_password',
        )

        # Other member
        self.other_member = self.user_model.objects.create_user(
            'other_member',
            'other_member@example.com',
            'other_member_password',
        )

        # Create a test Topic category
        self.test_category = TopicCategory.objects.create(
            title='Test category',
            created_by=self.moderator,
        )

    def test_access(self):
        """Test the view access to UserProfileUpdateView"""
        # Test access with not logged user
        test_access_logging_required(
            self,
            UserProfileUpdateView,
            reverse(
                'slow_forum:user-profile-edit-profile',
                kwargs={
                    'pk': self.moderator.pk,
                },
            ),
        )

        # Test if a member can access UserProfileUpdateView of its own
        # profile, and another user can't
        test_access_owner(
            self,
            reverse(
                'slow_forum:user-profile-edit-profile',
                kwargs={
                    'pk': self.member.pk,
                },
            ),
            self.other_member,
            self.member,
        )

        # Test if a moderation can access UserProfileUpdateView for a
        # member profile, based on its permission. And test that a
        # user not owner and without the right permission can't access
        # it.
        test_access_permission_required(
            self,
            reverse(
                'slow_forum:user-profile-edit-profile',
                kwargs={
                    'pk': self.member.pk,
                },
            ),
            self.other_member,
            self.moderator,
        )

    def test_edition_record(self):
        """Test edition is recorded

        Test that, after user profile edition:
        * edited field is set at true
        * edited_by field user is the same as the user who edited the profile
        * edited_on field is set
        """
        # Create a new user
        created_user = self.user_model.objects.create_user(
            'temp_member',
            'temp_member@example.com',
            'temp_member_password',
        )

        test_object_edition_record_with_view(
            self,
            created_user.profile,
            created_user,
            reverse(
                'slow_forum:user-profile-edit-profile',
                kwargs={
                    'pk': created_user.profile.pk,
                },
            ),
            {
                'nickname': 'My new nickname',
                'abstract': 'I will not be here long',
                'external_link_set-TOTAL_FORMS': 1,
                'external_link_set-INITIAL_FORMS': 0,
                'external_link_set-0-label': '',
                'external_link_set-0-url': '',
                'external_link_set-0-priority': 0,
            },
            302,
        )

        # Check that the sent data are recorder on the profil
        self.assertEqual(
            created_user.profile.nickname,
            'My new nickname',
            msg='New nickname is not set correctly',
        )
        self.assertEqual(
            created_user.profile.abstract,
            'I will not be here long',
            msg='New abstract is not set correctly',
        )

        # Delete the created user
        created_user.delete()
