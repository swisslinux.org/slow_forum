from django.utils import html

import markdown

from .utils import (
    shift_html_heading,
)

from .conf import (
    SLOW_MESSAGE_TO_HTML,
)


def markdown_to_html(markdown_text, use_config=True):
    """Convert markdown to html, but escape HTML before

    Parameters:
    - markdown_text (str): The Markdow text to convert
    - use_default (bool): If use the config set by user

    Return:
    The HTML converted text
    """
    offset = 2
    extensions_list = []
    extensions_config_dict = {}

    # Set extensions list and config
    if use_config:
        # Get Offset
        offset = SLOW_MESSAGE_TO_HTML.get('HEADING_OFFSET', 2)

        # Get TOC status
        if SLOW_MESSAGE_TO_HTML.get('ENABLE_TOC', False):
            extensions_list.append('markdown.extensions.toc')
            extensions_config_dict['markdown.extensions.toc'] = {}
            
        # Get code highlight status
        if SLOW_MESSAGE_TO_HTML.get('ENABLE_CODE_HIGHLIGHT', False):
            extensions_list.append('markdown.extensions.codehilite')
            extensions_config_dict['markdown.extensions.codehilite'] = {}

    # Exscape the HTML already in the markdown, for security
    escaped_markdown = html.escape(markdown_text)

    # Convert to HTML
    html_text = markdown.markdown(
        text=escaped_markdown,
        extensions=extensions_list,
        extension_configs=extensions_config_dict,
        output_format="html",
    )
    
    return shift_html_heading(
        html_text=html_text,
        offset=offset,
    )
