from slow_forum.models import Banner


def banners(request):
    """Return all the banners to be displayed"""
    return {
        'banners': Banner.get_currently_displayed()
    }

