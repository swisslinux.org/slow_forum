from django.contrib.auth import get_user_model

from slow_forum.models import UserProfile


# Signal handling for model save
# Only used to create a profile for each new user

def model_post_save_callback(sender, instance, created, **kwargs):
    """Mode post save Callback"""
    # Check if the model is a user
    if sender == get_user_model():
        # If profile do not exist
        if created:
            UserProfile.objects.create(user=instance)
