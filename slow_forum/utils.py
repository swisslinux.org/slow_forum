import re

MAX_HTML_HEADING_LEVEL = 6


def shift_html_heading(html_text, offset):
    """Shift the HTML heading (h1, h2, etc) with an offset

    Parameters:
    - html_text (str): The HTML text to apply heading offset on
    - offset (int): The offset to apply

    Return:
    An HTML text with the heading shifted with the given offset
    """
    new_html_text = html_text
    
    for level in range(MAX_HTML_HEADING_LEVEL, 0, -1):
        # Set the new level
        new_level = level+offset

        # Check if new level are out of maximum level
        if new_level > MAX_HTML_HEADING_LEVEL:
            # We replace this heading level with div
            new_html_tag_open = '<div'
            new_html_tag_close = '</div>'
        else:
            # We replace this heading level the offset
            new_html_tag_open = f'<h{new_level}'
            new_html_tag_close = f'</h{new_level}>'

        # Do the substitution
        new_html_text = re.sub(
            pattern=f'<h{level}',
            repl=new_html_tag_open,
            string=new_html_text,
        )
        new_html_text = re.sub(
            pattern=f'</h{level}>',
            repl=new_html_tag_close,
            string=new_html_text,
        )
        
    return new_html_text
