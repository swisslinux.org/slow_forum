from django.utils.translation import gettext_lazy as _
from django.forms import (
    inlineformset_factory,
    ModelForm,
    Form,
    HiddenInput,
    TextInput,
    Textarea,
    NumberInput,
    URLInput,
    Select,
    PasswordInput,
    CharField,
)
from django.contrib.auth import password_validation
from django.contrib.auth.forms import (
    AuthenticationForm,
    UsernameField,
    PasswordChangeForm,
)

import shlex

from slow_forum.models import (
    Topic,
    TopicReply,
    TopicCategory,
    UserProfile,
    UserProfileExternalLinks,
)


# Forms for slow_forum

class TopicReplyForm(ModelForm):
    """A model form for TopicReply"""
    
    class Meta:
        model = TopicReply
        fields = ('message',)


class SearchForm(Form):
    """A simple search form"""
    q = CharField(
        label=_('Query'),
        max_length=255,
        required=False,
    )

    def get_search_query_splited(self):
        """Return the search query, splitted"""
        if self.is_valid():
            return shlex.split(
                self.cleaned_data['q'],
            )
        else:
            return []
            
