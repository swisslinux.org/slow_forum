"""slow_message: Template tags and filters for render message forum"""

from django import template
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


register = template.Library()


@register.inclusion_tag(
    'slow_forum/includes/content/message.html',
    takes_context=True,
)
def topic_message(context, topic):
    """Render the message from a topic"""
    # Tell if show extra edition info
    view_extra_info_perm = 'view_topic_extra_edition_info'
    if context['perms']['slow_forum'][view_extra_info_perm]:
        show_extra_edition_infos = True
    else:
        show_extra_edition_infos = False

    # Tell if show username
    if context['perms']['slow_forum']['view_user']:
        show_username = True
    else:
        show_username = False

    return {
        'message_html': topic.message_html,
        'created_by': topic.created_by,
        'created_on': topic.created_on,
        'edited': topic.edited,
        'edited_by': topic.edited_by,
        'edited_on': topic.edited_on,
        'actions': False,
        'edit_url': None,
        'delete_url': None,
        'show_username': show_username,
        'show_extra_edition_infos': show_extra_edition_infos,
        'aria_description': _('Topic message'),
    }

@register.inclusion_tag(
    'slow_forum/includes/content/message.html',
    takes_context=True,
)
def topic_reply_message(context, topic_reply):
    """Render the message from a topic reply"""
    # Tell if show extra edition info
    view_extra_info_perm = 'view_topicreply_extra_edition_infos'
    if context['perms']['slow_forum'][view_extra_info_perm]:
        show_extra_edition_infos = True
    else:
        show_extra_edition_infos = False

    # Tell if show username
    if context['perms']['slow_forum']['view_user']:
        show_username = True
    else:
        show_username = False

    # Tell if edition is allowed or owner
    edition_allowed = context['perms']['slow_forum']['change_topicreply']
    owner = topic_reply.is_owner(context['user'])

    # If owner or edition allowed, set edit URL
    if edition_allowed or owner:
        edit_url = reverse(
            'slow_forum:topic-reply-update',
            kwargs={
                'topic_pk': topic_reply.topic.pk,
                'pk': topic_reply.pk,
            },
        )
    else:
        edit_url = None

    # Tell if delete is allowed
    delete_allowed = context['perms']['slow_forum']['change_topicreply']

    # If  allowed, set delete URL
    if delete_allowed:
        delete_url = reverse(
            'slow_forum:topic-reply-delete',
            kwargs={
                'topic_pk': topic_reply.topic.pk,
                'pk': topic_reply.pk,
            },
        )
    else:
        delete_url = None

    # Define HTML id
    html_id = f'reply_{topic_reply.pk}'
        
    return {
        'message_html': topic_reply.message_html,
        'html_id': html_id,
        'absolute_url': topic_reply.get_absolute_url(),
        'created_by': topic_reply.created_by,
        'created_on': topic_reply.created_on,
        'edited': topic_reply.edited,
        'edited_by': topic_reply.edited_by,
        'edited_on': topic_reply.edited_on,
        'actions': True,
        'edit_url': edit_url,
        'delete_url': delete_url,
        'show_username': show_username,
        'show_extra_edition_infos': show_extra_edition_infos,
        'aria_description': _('Reply message'),
    }
