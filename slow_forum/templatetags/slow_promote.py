"""slow_promote: Template tags and filters for promote content"""

from django import template
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


register = template.Library()


@register.inclusion_tag(
    'slow_forum/includes/content/promote_topic.html'
)
def promote_topic(topic):
    """Render an element that promote a topic"""
    return {
        'subject': topic.subject,
        'created_on': topic.created_on,
        'created_by': topic.created_by,
        'category': topic.category,
        'last_message_on': topic.last_message_on,
        'replies_count': topic.replies_count,
        'absolute_url': topic.get_absolute_url(),
    }


@register.inclusion_tag(
    'slow_forum/includes/content/promote_topic_reply.html'
)
def promote_topic_reply(topic_reply):
    """Render an element that promote a topic reply"""
    return {

        'created_on': topic_reply.created_on,
        'created_by': topic_reply.created_by,
        'topic': topic_reply.topic,
        'absolute_url': topic_reply.get_absolute_url(),
    }


@register.inclusion_tag(
    'slow_forum/includes/content/promote_profile.html'
)
def promote_profile(profile, include_nickname=True):
    """Render an element that promote a profile"""
    return {
        'nickname': profile.nickname,
        'include_nickname': include_nickname,
        'picture': None,
        'absolute_url': profile.get_absolute_url(),
    }
