"""slow_forms: Template tags and filters for forms in Slow Forum"""

from django.shortcuts import render
from django import template
from django.utils.translation import gettext_lazy as _

register = template.Library()


def bootstrapify_form(form, input_css_class='form-control'):
    """Modify a form to be ready to use with Bootstrap"""
    for bound_field in form.visible_fields():

        # If bound field is a check_box
        if bound_field.widget_type == 'checkbox':
            html_class_to_add = 'form-check-input'
        else:
            html_class_to_add = input_css_class
        
        # Add 'form-input' HTML class into attrs
        field_html_class = bound_field.field.widget.attrs.get(
            'class',
            '',
        )
        field_html_class = '{} {}'.format(
            html_class_to_add,
            field_html_class,
        )
            
        bound_field.field.widget.attrs['class'] = field_html_class

    return form


@register.inclusion_tag(
    'slow_forum/includes/form/bootstrap_form.html'
)
def bootstrap_form(form, input_css_class='form-control'):
    """Render a bootstrap form for the given form"""
    return {
        'form': bootstrapify_form(form, input_css_class),
    }


@register.inclusion_tag(
    'slow_forum/includes/form/bootstrap_form_grouped_fields.html'
)
def bootstrap_form_grouped_fields(form, input_css_class='form-control'):
    """Render a bootstrap form for the given form, but group the fields"""
    return {
        'form': bootstrapify_form(form, input_css_class),
    }

@register.inclusion_tag(
    'slow_forum/includes/form/bootstrap_form_search.html'
)
def bootstrap_form_search(form, input_css_class='form-control'):
    """Renmder a bootstrap form for a searc form"""
    form = bootstrapify_form(form, input_css_class)
    query_field = form['q']

    # Add aria information and placeholder for query_field
    query_field.field.widget.attrs['placeholder'] = _('Query')
    query_field.field.widget.attrs['aria-label'] = _('Search query')
    query_field.field.widget.attrs['aria-description'] = _(
        'Write your search query here',
    )

    return {
        'query_field': query_field,
        'form': form,
    }
    
