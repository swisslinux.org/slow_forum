from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.urls import reverse
from django.core.validators import (
    EmailValidator,
    URLValidator,
)

from .convert import markdown_to_html

EXTERNAL_LINK_ALLOWED_SCHEMES = (
    'https',
    'xmpp',
    'matrix',
    'ircs',
    'irc',
    'mailto',
)


# Abstract models

class EditableModel(models.Model):
    """An editable Model"""
    edited = models.BooleanField(
        default=False,
        verbose_name=_('edited'),
    )
    edited_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='+',
        blank=True,
        null=True,
        verbose_name=_('edited_by'),
    )
    edited_on = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_('edited_on'),
    )

    class Meta:
        abstract = True

    def record_edition(self, by_user, on_datetime):
        """Recorde an edition by a user"""
        self.edited = True
        self.edited_by = by_user
        self.edited_on = on_datetime


class FormatedMessage(models.Model):
    """A Message formated with Markdown"""
    message = models.TextField(
        verbose_name=_('message'),
        help_text=_('You can use Markdown')
    )
    message_html = models.TextField(
        blank=True,
        null=True,
        verbose_name=_('message_html'),
        help_text=_(
            'HTML version of the message, automatically generated on save'
        ),
    )

    class Meta:
        abstract = True

    def generate_message_html(self):
        """Generate message_html field by convert message field"""
        self.message_html = markdown_to_html(self.message)

    def save(self, *args, **kwargs):
        """Override save method"""
        # Generate the html version of the message
        self.generate_message_html()
        return super(FormatedMessage, self).save(*args, **kwargs)


# Models

class UserProfile(EditableModel):
    """A user profile, linked to a User account"""
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='profile',
        verbose_name=_('user'),
    )
    nickname = models.CharField(
        max_length=255,
        null=True,
        unique=True,
        verbose_name=_('nickname'),
    )
    abstract = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('abstract'),
    )

    class Meta:
        ordering = ('nickname',)
        verbose_name = _('User profile')
        verbose_name_plural = _('User profiles')
        permissions = [
            (
                'view_user_profile_edition_infos',
                _('Can view user profile infos related to edition'),
            ),
        ]

    def __str__(self):
        return self.nickname or 'Nickname not defined'

    def get_absolute_url(self):
        """Get the absolute URL of an instance"""
        return reverse(
            'slow_forum:user-profile-detail',
            kwargs={'pk': self.pk},
        )

    def latest_topic_set(self, count=5):
        """Get the latest topics

        Property:
        * count (int): How many latest topic in max

        Return:
        (queryset): The latest topics
        """
        return self.user.topic_set.order_by(
            '-created_on',
        ).order_by(
            '-pk',
        )[:count]

    def latest_topicreply_set(self, count=5):
        """Get the latest topics

        Property:
        * count (int): How many latest topic in max

        Return:
        (queryset): The latest topics
        """
        return self.user.topicreply_set.order_by(
            '-created_on',
        ).order_by(
            '-pk',
        )[:count]
    
    @classmethod
    def create_missing(cls, user_queryset):
        """Create the missing profiles for Users

        Parameter:
        * user_queryset: The User queryset on which create profile

        Return:
        * (int): The number of user for which a profile has been created
        """
        # Filter queryset, to be sure we only have users with no profiles
        filtered_user_queryset = user_queryset.filter(profile__isnull=True)

        # Check we still have Users
        if len(filtered_user_queryset) == 0:
            return 0

        # If still Users
        for user in filtered_user_queryset:
            cls.objects.create(user=user)

        # Return how many users was missing profiles
        return len(filtered_user_queryset)


class UserProfileExternalLinks(models.Model):
    """An extrenal link, related to a profile"""
    user_profile = models.ForeignKey(
        UserProfile,
        on_delete=models.CASCADE,
        related_name='external_link_set',
        verbose_name=_('user profile'),
    )
    priority = models.IntegerField(
        default=0,
        verbose_name=_('priority'),
    )
    label = models.CharField(
        max_length=255,
        verbose_name=_('label'),
    )
    url = models.URLField(
        max_length=200,
        validators=(
            URLValidator(schemes=EXTERNAL_LINK_ALLOWED_SCHEMES),
        ),
        verbose_name=_('URL'),
    )

    class Meta:
        ordering = ('priority',)
        verbose_name = _('User profile extrenal link')
        verbose_name_plural = _('User profile extrenal links')

    def __str__(self):
        """Get the str version"""
        return f'{self.label}'

    def get_absolute_url(self):
        """Get the absolute URL of an instance"""
        return self.user_profile.get_absolute_url()


class TopicCategory(EditableModel):
    """A topics category"""
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_('created_by'),
    )
    created_on = models.DateTimeField(
        default=timezone.now,
        verbose_name=_('created_on'),
    )
    title = models.CharField(
        max_length=255,
        unique=True,
        verbose_name=_('title'),
    )
    priority = models.IntegerField(
        default=0,
        verbose_name=_('priority'),
    )
    short_description = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_('short description'),
        help_text=_('A few words, shown in categories list'),
    )
    full_description = models.TextField(
        blank=True,
        null=True,
        verbose_name=_('full description'),
        help_text=_('Shown in category detail view'),
    )

    class Meta:
        ordering = ('priority',)
        verbose_name = _('topic category')
        verbose_name_plural = _('topic categories')
        permissions = [
            (
                'view_topiccategory_metadata',
                _('Can view topic category metadata'),
            ),
        ]

    def __str__(self):
        return self.title

    @property
    def topics_count(self):
        """Return the count of topics linked to this category"""
        return self.topics_set.count()

    @property
    def replies_count(self):
        """Return the count of replies in this category"""
        return sum(
            [
                topic.replies_count
                for topic
                in self.topics_set.all()
            ]
        )

    @property
    def messages_count(self):
        """Return the count of all messages in this category

        Note: Each topic and each reply have 1 message
        """
        return self.topics_count + self.replies_count

    @property
    def latest_topic(self):
        """Return the latest topic in this category"""
        if self.topics_set.exists():
            return self.topics_set.latest('created_on')
        else:
            return None
        
    @property
    def latest_reply(self):
        """Return the latest reply in this category"""
        if self.topics_set.exists():
            # Get latest reply of each topics
            topics_latest_replies = [
                topic.latest_reply
                for topic
                in self.topics_set.all()
                if topic.latest_reply is not None
            ]

            # If get no latest replies, return None
            if len(topics_latest_replies) == 0:
                return None

            # Sort the messages
            sorted_topic_replies = sorted(
                topics_latest_replies,
                key=lambda reply: reply.created_on,
                reverse=True,
            )
            return sorted_topic_replies[0]
        else:
            return None

    @property
    def latest_message_created_on(self):
        """Return the latest message created_on value

        Note: Each topic and each reply have 1 message
        """
        # Get latest topic and reply
        latest_topic = self.latest_topic
        latest_reply = self.latest_reply

        if latest_topic and not latest_reply:
            return latest_topic.created_on

        if latest_reply and not latest_topic:
            return latest_reply.created_on

        if not latest_reply and not latest_topic:
            return None

        if latest_topic.created_on > latest_reply.created_on:
            return latest_topic.created_on

        return latest_reply.created_on

    def get_absolute_url(self):
        """Get the absolute URL of a model instance"""
        return reverse(
            'slow_forum:topic-category-details',
            kwargs={'pk': self.pk},
        )


class Topic(FormatedMessage, EditableModel):
    """A topic, to talk"""
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_('created_by'),
    )
    created_on = models.DateTimeField(
        default=timezone.now,
        verbose_name=_('created_on'),
    )
    subject = models.CharField(
        max_length=255,
        verbose_name=_('subject'),
    )
    category = models.ForeignKey(
        TopicCategory,
        on_delete=models.CASCADE,
        related_name='topics_set',
        verbose_name=_('category'),
    )
    last_message_on = models.DateTimeField(
        null=True,
        editable=False,
        verbose_name=_('Last message on'),
    )

    class Meta:
        ordering = ('created_on',)
        verbose_name = _('topic')
        verbose_name_plural = _('topics')
        permissions = [
            (
                'view_topic_extra_edition_infos',
                _('Can view topic extra infos related to edition'),
            ),
        ]

    def __str__(self):
        return self.subject

    def save(self, *args, **kwargs):
        """Override save, to update last_message_on"""
        # If object is new
        if not self.pk:
            self.last_message_on = self.created_on

        # Call
        return super().save(*args, **kwargs)

    @property
    def replies_count(self):
        """Return the count of replies linked to this topic"""
        return self.replies_set.count()

    @property
    def latest_reply(self):
        """Return the latest reply"""
        if self.replies_set.exists():
            return self.replies_set.latest('created_on')
        else:
            return None

    def update_last_message_on(self):
        """Update last_message_on"""
        # Check if replies
        if self.replies_count == 0:
            self.last_message_on = self.created_on
        else:
            self.last_message_on = self.latest_reply.created_on

    def is_owner(self, user):
        """Is the given user owner of this topic"""
        return self.created_by == user

    def get_absolute_url(self):
        """Get the absolute URL of a model instance"""
        return reverse('slow_forum:topic-detail', kwargs={'pk': self.pk})


class TopicReply(FormatedMessage, EditableModel):
    """A topic reply, linked to a Topic"""
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_('created_by'),
    )
    created_on = models.DateTimeField(
        default=timezone.now,
        verbose_name=_('created_on'),
    )
    topic = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        related_name='replies_set',
        verbose_name=_('topic'),
    )

    class Meta:
        ordering = ('created_on',)
        verbose_name = _('Topic reply')
        verbose_name_plural = _('Topic replies')
        permissions = [
            (
                'view_topicreply_extra_edition_infos',
                _('Can view topic reply extra infos related to edition'),
            ),
        ]

    def __str__(self):
        return f'Topic reply by {self.created_by.username}, {self.created_on}'

    def save(self, *args, **kwargs):
        """Override save to update topic.last_message_on"""
        # Save the reply
        super().save(*args, **kwargs)

        # Update the topic last_message_on
        self.topic.update_last_message_on()
        self.topic.save()
    
    def is_owner(self, user):
        """Is the given user owner of this topic"""
        return self.created_by == user

    def get_absolute_url(self):
        """Get the absolute URL of a model instance"""
        return '{url}#reply_{reply_pk}'.format(
            url=self.topic.get_absolute_url(),
            reply_pk=self.pk,
        )

    
class Banner(FormatedMessage, models.Model):
    """A banner, seen on every content pages"""
    title = models.CharField(
        max_length=255,
        verbose_name=_('title'),
    )
    priority = models.IntegerField(
        default=0,
        verbose_name=_('priority'),
    )
    display_from = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_('display from'),
    )
    display_to = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_('display to'),
    )

    class Meta:
        ordering = ('priority',)
        verbose_name = _('banner')
        verbose_name_plural = _('banners')

    def __str__(self):
        return self.title

    @classmethod
    def get_currently_displayed(cls):
        """Return the banner to be currently displayed"""
        return cls.objects.exclude(
            display_from__gt=timezone.now(),
        ).exclude(
            display_to__lte=timezone.now(),
        )

    @classmethod
    def delete_expired(cls, expired_from=None, queryset=None):
        """Delete all banners with display_to expired

        Parameters:
        * expired_from: Datetime for the expiration
        * queryset: The queryset to apply deletion of epired baners
        """
        if not expired_from:
            expired_from = timezone.now()
        if not queryset:
            queryset = cls.objects.all()
            
        return queryset.filter(
                display_to__lte=expired_from,
            ).delete()
    
