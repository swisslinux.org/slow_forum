from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from slow_forum.models import UserProfile


class Command(BaseCommand):
    """Create all missing User Profiles"""
    help = 'Create all missing User Profiles'

    def handle(self, *args, **options):
        """Handle the command call"""
        # For each user with no profile
        users_with_no_profiles = get_user_model().objects.filter(
            profile__isnull=True
        )

        if len(users_with_no_profiles) == 0:
            self.stdout.write(
                self.style.SUCCESS(
                    'No missing User Profiles'
                    )
                )
            return
        
        users_count = UserProfile.create_missing(users_with_no_profiles)

        self.stdout.write(
            self.style.SUCCESS(
                'Successfully create {} missing User Profile(s)'.format(
                    users_count,
                )
            )
        )

