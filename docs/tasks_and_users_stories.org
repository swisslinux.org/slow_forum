#+TITLE: Tasks planned for this project


* Users stories

** Step 1

*** DONE Create a discussion

Who: User

Want to: Start a discussion with a first message and a subject.


*** DONE Reply to a discussion

Who: User

Want to: Reply to a message, in a discussion


*** DONE See other users

Who: User, admins

Want to: See other users

*** TODO Moderate a content

Who: Moderators and admins

Want to: Delete problematic content or user account


*** TODO Basic role system

Who: Moderators and admins

Want to: Attribute role(s) to each user


*** TODO Basic permission system

Who: Moderators and admins

Want to: Set what each role can do


*** TODO Registration of new user

Who: Users

Want to: Register on the forum


** Step 2

*** TODO Message rendering preview

Who: User

Want to: See how its message will look like


*** TODO Reply following system

Who: User

Want to: Easy see which message reply to which message


*** TODO Search discussion or message

Who: User

Want to: Do search in forum content about a subject


*** TODO Settings modifiable by admins

Who: Admins

Want to: Modify forum settings from the Forum UI

*** TODO User profile

Who: User

Want to: Edit its profile (picture, name, etc)


*** TODO Categories

Who: Admins

Want to: Create categories of discussions


*** TODO Limit access to some categories

Who: Admins

Want to: Limit access to some categories to some users


** Step 3

*** TODO Direct discussion between multiple users

Who: User, moderators

Whant to: Exchange direct messages between users


*** TODO Promote new message

Who: User

Want to: Better see new message in a discussion, if they visite a discussion more than one time


*** TODO Anti-spam system

Who: Admins/moderators

Want to: Avoid spams, in user names and content


*** TODO Report system

Who: Users

Want to: Report contents and users to moderation teams, so moderators
can take action


*** TODO Notification

Who: Users and moderators

Want to: Be notified when a discussion have a response or categories
have new discussions


** Step 4

*** TODO Banner

Who: Admins and moderatiors

Want to: Have a banner on top of the forum for important message

*** TODO Mark a discussion as solved

Who: Users

Want to: Mark a discussion as solved, and mark the message who solved
it so other user can see it.


*** TODO Poll

Who: Users

Want to: Attach a poll to a discussion. The choices can be words, or date and time.


*** TODO Badges

Who: Users

Want to: Have some badge, to mark certains occasions


*** TODO API

Who: Admins

Whant to: Have an API, to create new discussion from another app.
Example: Create a new discussion when the blog have a new post


* Tasks

** TODO Enhance accessibility

Until now, to keep a good accessibility, I do this:
- Keep the HTML structure simple
- Use "aria-description" or "title" attribute for purely visual elements
- Always link form field with its label
- Use the Bootsrap elements and colors
- When use custom elements, keep a good structure and use enough space

But I still have things to learn.
So, I go learn and come back to enhance this forum accessibility.

** TODO Build breadcrumb programitically

Breadcrumb should be:
- Defined at the view class level
- Available to the template by the context
- Built on the base_content template level, the same way for every pages
  

** TODO Move feeds

- For now, RSS feeds are on their own page.
- Remove this page
- On index, put feeds for all topics and replies
- On a category, put  topics and replies feeds for this category

** TODO Add feed in HTML header

** TODO Test the RSS feeds

** TODO Add command and admin action to delete account not active and expired token

** TODO Add a markdown to html template filter

** TODO Add markdown support for TopicCategory.full_description

** TODO Find what activity to put on index

** TODO Add a profile picture

And use it everywhere necessary

** TODO Add paginations

For lists of:
- Topic: In category detail and user profile detail
- Replies: In user profile detail
- User profiles: In user profile list

** TODO Add possibility to move one or many topics to another category

** TODO Add more notification messages

Like:
- Error messages in case of error in create/edit/delete views
- User group edition: Notify the user he/she is part of a new group,
  or removed from a group
- See for other notifications

** TODO Cut some templates parts into snippets

Example:
- Topic message
- Topic reply
- List of topics
- List of replies on multiple topics
  

** IN-PROGRESS Test the access restrictions on restricted views

** TODO Add view for user registration

With e-mail validation

*** TODO Set the nickname

- Ask to user when account creation
- And explain clearly the difference between nickname and username

** TODO Make sure the topic reply are correctly rendered in admin

** TODO Add a Markdown editor for each markdown fields

- In my views
- In admin
- Maybe make a new type of field ?
  

** TODO Fix double char escape with markdon codeblock to HTML

** TODO Document the command "create_missing_user_profiles"

** TODO Use a homemade template tag for conversion of Markdown to HTML

** TODO Test RSS feeds

- Make sure their return a safe value, corresponding to what we want
