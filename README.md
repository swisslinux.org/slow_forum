# Slow forum #

Is a simple, minimalist, forum web application.


## IMPORTANT ##

The development are paused. 

I still have things to learn about accessibility and I will come back
on the dev after I have learned more.

This is a not ready to use application. We made it on our free time,
at the best we can.


## Features ##

Nothing for now


## Why another forum application ##

At Swisslinux.org, we have tested differents forum web app. But we
cannot found something that correpsond to our needs. So we made one.


## What you found here ##

In this repository, you found 2 source code:
- `slow_forum` a Django application
- `slow_forum_site` an exemple of Django project that use the
  `slow_forum` application
  
Later, the `slow_forum` application will be packaged on pypi.org. And
the documentation on how to integrate it on your Django project will
be written.

`slow_forum_site` is a ready to use Django project. We use it for
developpement, but you can use it as your website.


## Documentation ##

### Install for dev ###

To be written.


### Install for production ###

To be written.


## Licence ##

Licences of this software:
- Slow forum: GPLv3 or later.

And its direct dependencies:
- Bootstrap: MIT
- Django: BSD 3-clauses
- django-extra-views: MIT
- django-dynamic-preferences: BSD-3-Clause license
- django-registration: BSD-3-Clause license 
- Python-Markdown: BSD


## Author ##

Sébastien Gendre <seb@k-7.ch>

