from django.shortcuts import render

from django_registration.backends.activation.views import (
    RegistrationView,
)

from slow_forum_registration.forms import CustomRegistrationForm


class CustomRegistrationView(RegistrationView):
    """Custom RegistrationView

    Modifications:
    * Use CustomRegistrationForm as form_class
    * Override register() to set the profile nickname after user creation"""

    form_class = CustomRegistrationForm
    
    def register(self, form):
        """Override register(), to set the profile nickname after user creation"""
        new_user = super().register(form)
        profile = new_user.profile
        profile.nickname = form.cleaned_data['profile_nickname']
        profile.save()
        return new_user

