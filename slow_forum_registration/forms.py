from django.utils.translation import gettext_lazy as _
from django.forms import (
    CharField,
)

from django_registration.forms import (
    RegistrationForm
)


# Forms for account registration

class CustomRegistrationForm(RegistrationForm):
    """Override RegistrationForm to add profile_nickname field"""
    profile_nickname = CharField(
        label=_('Profile nickname'),
        help_text=_(
            'Nickname is how other people see you.',
        ),
    )

    class Meta(RegistrationForm.Meta):
        help_texts = {
            'username': _('Username is used to login.')
        }
