from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model


# View tests

class CustomRegistrationViewTest(TestCase):
    """Test the view CustomRegistrationView"""
    
    def setUp(self):
        """Set up the test"""
        self.client = Client()
        self.user_model = get_user_model()
    
    def test_register(self):
        """Test the register() method override"""
        # Be sure to be logout
        self.client.logout()
        
        # Do request on register view
        self.client.post(
            reverse(
                'django_registration_register',
            ),
            {
                'username': 'test_account_registration',
                'email': 'test@example.com',
                'password1': 'test_password',
                'password2': 'test_password',
                'profile_nickname': 'test_nickname',
            },
        )

        # Get the created user
        created_user = self.user_model.objects.get(
            username='test_account_registration',
        )

        # Test if its profile nickname correspond the the one given on
        # the request
        self.assertEqual(
            created_user.profile.nickname,
            'test_nickname'
        )

        # Delete the created user
        created_user.delete()

