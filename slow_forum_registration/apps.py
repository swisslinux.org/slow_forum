from django.apps import AppConfig


class SlowForumRegistrationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "slow_forum_registration"
    verbose_name = "Slow forum registration customisations"
